#include "articulated_instrument.h"

#include <sofa/simulation/graph/SimpleApi.h>
#include <sofa/component/statecontainer/MechanicalObject.h>
#include <sofa/core/CollisionModel.h>

#include "physics/physicalbody.h"
#include "visual/visualbody.h"
#include "visual/mesh/visualmeshimporter.h"
#include "physics/articulation.h"
#include "simulation/simulation.h"

namespace filasofia {

namespace prefabs {

using sofa::simpleapi::createChild;
using sofa::simpleapi::createObject;

using ArticulationDof = sofa::component::statecontainer::MechanicalObject<sofa::defaulttype::Vec1Types>;
typedef sofa::helper::WriteAccessor<sofa::Data<ArticulationDof::VecCoord>> ArticulationWriteAccessor;
typedef sofa::helper::ReadAccessor<sofa::Data<ArticulationDof::VecCoord>> ArticulationReadAccessor;

using std::to_string;

void ArticulatedInstrument::setOpenning(float value)
{
	value = std::clamp(value, MinOpenning, MaxOpenning);
	if (m_openning == value) return;
	m_openning = value;
    move(ArticulationRole::LeftJawOpenning, value);
    move(ArticulationRole::RightJawOpenning, value);
}

void ArticulatedInstrument::setInsertion(float value)
{
	if (m_insertion == value) return;
	m_insertion = value;
	move(ArticulationRole::Insertion, value);
}

void ArticulatedInstrument::setRotation(float value)
{
	if (m_rotation == value) return;
	m_rotation = value;
    move(ArticulationRole::Rotation, value);
}

ArticulatedInstrument::ArticulatedInstrument(const std::string &name) :
    m_name {name}
{
}

void ArticulatedInstrument::move(ArticulationRole role, float value)
{
    ArticulationWriteAccessor dof = m_articulation
        ->node()
        ->get<ArticulationDof>()
        ->write(sofa::core::VecId::restPosition());

    dof[static_cast<int>(role)].at(0) = value;
}

ArticulatedInstrument::Builder::Builder(const std::string &name)
{
    m_instrument.reset(new ArticulatedInstrument(name));
}

ArticulatedInstrument::Builder & ArticulatedInstrument::Builder::addPart(
    PartRole role, const PartConfiguration &config) 
{
    m_parts[role] = config;
    return *this;
}

ArticulatedInstrument::Builder & ArticulatedInstrument::Builder::attach(
    const std::shared_ptr<PhysicalBody> &body, 
    const Eigen::Vector3d &offset) 
{ 
    m_attachmentBody = body; 
    m_attachmentOffset = offset;
    return *this;
}

std::shared_ptr<ArticulatedInstrument> ArticulatedInstrument::Builder::build(
    Simulation *sim)
{
    Articulation::JointConfiguration shaftTranslation = {
        .type = Articulation::JointType::Prismatic,
        .parentOffset = m_attachmentOffset,
        .childOffset = {0, 0, 0},
        .axis = {0, 0, 1},
        .stiffness = ShaftStiffness,
        .contactCompliance = ShaftContactCompliance,
        .childIndex = static_cast<int>(ArticulationRole::Insertion) + 1
    };
    
    Articulation::JointConfiguration shaftRotation = {
        .type = Articulation::JointType::Revolute,
        .axis = {0, 0, 1},
        .stiffness = ShaftStiffness,
        .contactCompliance = ShaftContactCompliance,
        .parentIndex = static_cast<int>(ArticulationRole::Insertion) + 1,
        .childIndex = static_cast<int>(ArticulationRole::Rotation) + 1
    };

    Articulation::JointConfiguration leftJawRotaiton = {
        .type = Articulation::JointType::Revolute,
        .axis = {0,1,0},
        .stiffness = JawStiffness,
        .contactCompliance = JawContactCompliance,
        .parentIndex = static_cast<int>(ArticulationRole::Rotation) + 1,
        .childIndex = static_cast<int>(ArticulationRole::LeftJawOpenning) + 1
    };

    Articulation::JointConfiguration rightJawRotation = {
        .type = Articulation::JointType::Revolute,
        .axis = {0,-1,0},
        .stiffness = JawStiffness,
        .contactCompliance = JawContactCompliance,
        .parentIndex = static_cast<int>(ArticulationRole::Rotation) + 1,
        .childIndex = static_cast<int>(ArticulationRole::RightJawOpenning) + 1
    };

    PhysicalBody::MechanicalModelParams mechanicalParams;
    mechanicalParams.type = PhysicalBody::BodyType::Rigid;
    mechanicalParams.numberOfElements = NumberOfDofs;
    mechanicalParams.solverParams.linearSolver = PhysicalBody::SolverType::None;

    m_instrument->m_proxy = sim->physicsServer()
        .addBody(PhysicalBody::Builder(std::string("Forceps"))
            .mechanicalModel(mechanicalParams));

    m_instrument->m_articulation = sim->physicsServer()
        .addArticulation(Articulation::Builder("ForcepsArticulation")
            .joint(shaftTranslation)
            .joint(shaftRotation)
            .joint(leftJawRotaiton)
            .joint(rightJawRotation)
            .controlledBody(m_instrument->m_proxy)
            .attachedTo(m_attachmentBody));

    for (const auto &[role, part] : m_parts) {
        VisualBody::Builder vBuilder(PartNames.at(role));
        vBuilder.material(part.material).mesh(importVisualMesh(part.visualMesh));

        for (auto &[name, texture] : part.textures) {
            vBuilder.addTexture(name, texture);
        }

        m_instrument->m_parts[role] = sim->visualServer().addVisualBody(vBuilder);

        PhysicalBody::MechanicalModelParams mechanicalParams;
        mechanicalParams.type = PhysicalBody::BodyType::Rigid;
        mechanicalParams.solverParams.linearSolver = PhysicalBody::SolverType::None;
        mechanicalParams.mass = 0.05;

        PhysicalBody::CollisionModelParams collisionParams;
        collisionParams.group = part.collisionGroup;
        collisionParams.primitives = PhysicalBody::CollisionModelFlags::Line
                | PhysicalBody::CollisionModelFlags::Point;
        collisionParams.proximity = part.collisionProximity;

        auto pBody = sim->physicsServer().addBody(PhysicalBody::Builder(m_instrument->m_name + to_string(static_cast<int>(role)))
                                                  .map(m_instrument->m_proxy, static_cast<int>(role))
                                                  .mechanicalModel(mechanicalParams)
                                                  .collisionModel(collisionParams, part.collisionMesh)
                                                  );

        sim->map(pBody, m_instrument->m_parts[role]);
    }

    std::vector<std::shared_ptr<Transform>> visualBodies;
    for (auto &role: {PartRole::Shaft, PartRole::LeftJaw, PartRole::RightJaw}) {
        visualBodies.push_back(std::static_pointer_cast<Transform>(m_instrument->m_parts[role]));
    }

    return m_instrument;
}

}

}
