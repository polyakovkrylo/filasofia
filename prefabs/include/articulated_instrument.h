#pragma once

#include <unordered_map>
#include <string>
#include <memory>

#include <Eigen/Dense>

namespace filament {
class Material;
class Texture;
}

namespace filasofia {

class Simulation;
class Articulation;
class PhysicalBody;
class VisualBody;

namespace prefabs {
	class ArticulatedInstrument
	{
    public:
        inline float openning() { return m_openning; }
        inline float insertion() { return m_insertion; }
        inline float rotation() { return m_rotation; }

		void setOpenning(float closing);
		void setInsertion(float value);
		void setRotation(float radians);

        // starting from two to match the DoFs
        // of the proxy body
		enum class PartRole {
            Shaft = 2,
            LeftJaw,
            RightJaw
		};

        inline static const std::unordered_map<PartRole, std::string> PartNames {
            { PartRole::LeftJaw, "LeftJaw" },
            { PartRole::RightJaw, "RightJaw" },
            { PartRole::Shaft, "Shaft" }
        };

		struct PartConfiguration {
			std::string visualMesh;
            std::shared_ptr<filament::Material> material;
            std::unordered_map<std::string, std::shared_ptr<filament::Texture>> textures;
            std::string collisionMesh;
            float collisionProximity {0};
            float collisionGroup {0};
		};

		class Builder {
		public:
            Builder(const std::string &name);

			Builder & addPart(PartRole role, const PartConfiguration &config);

            Builder & attach(const std::shared_ptr<PhysicalBody> &body,
				const Eigen::Vector3d &offset = {0,0,0});

            std::shared_ptr<ArticulatedInstrument> build(Simulation *sim);

		private:
            std::shared_ptr<ArticulatedInstrument> m_instrument;
			std::unordered_map<PartRole, PartConfiguration> m_parts;
			std::shared_ptr<PhysicalBody> m_attachmentBody;
            Eigen::Vector3d m_attachmentOffset;
		};

		friend class ArticulatedInstrument::Builder;

        const std::shared_ptr<PhysicalBody> & proxyBody() { return m_proxy; }

	private:
		ArticulatedInstrument(const std::string &name);

        enum class ArticulationRole {
			Insertion,
            Rotation,
            RightJawOpenning,
			LeftJawOpenning,
		};

        void move(ArticulationRole role, float value);
		
        std::string m_name;

        std::unordered_map<PartRole, std::shared_ptr<VisualBody>> m_parts;

		std::shared_ptr<Articulation> m_articulation;
		std::shared_ptr<PhysicalBody> m_proxy;

		float m_openning {0.0f};
		float m_insertion {0.0f};
		float m_rotation {0.0f};

        static constexpr float MaxOpenning { 1.0f };
        static constexpr float MinOpenning { 0.0f };
        static constexpr int NumberOfDofs { 5 };
        static constexpr float JawStiffness {5e2};
        static constexpr float JawContactCompliance {6e2};
        static constexpr float ShaftStiffness {5e2};
        static constexpr float ShaftContactCompliance {50};
    };
}

}
