cmake_minimum_required(VERSION 3.16)

project("Filasofia" DESCRIPTION "SOFA/Filament simulation framework") 
set(CMAKE_CXX_STANDARD 20)

add_library(${PROJECT_NAME} SHARED)

# definitions of CMAKE_INSTALL_LIBDIR, CMAKE_INSTALL_INCLUDEDIR and others
include(GNUInstallDirs)

target_include_directories(${CMAKE_PROJECT_NAME}
    PRIVATE
    ${CMAKE_CURRENT_SOURCE_DIR}/src
    ${CMAKE_CURRENT_SOURCE_DIR}/prefabs/src
    ${PROJECT_SOURCE_DIR}/third_party/stb
    PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
    $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>
    )

add_subdirectory(src/physics)
add_subdirectory(src/common)
add_subdirectory(src/simulation)
add_subdirectory(src/visual)
add_subdirectory(prefabs)

find_package(Sofa.Simulation REQUIRED)
find_package(Sofa.Component REQUIRED)
target_link_libraries(${CMAKE_PROJECT_NAME} Sofa.Component Sofa.Simulation)

find_path(FILAMENT_PATH "filament")
target_link_directories(${CMAKE_PROJECT_NAME} PRIVATE ${FILAMENT_PATH}/lib/x86_64)
target_include_directories(${CMAKE_PROJECT_NAME}  PRIVATE ${FILAMENT_PATH}/include)
target_link_libraries(${CMAKE_PROJECT_NAME}
    filament
    basis_transcoder
    filamat
    shaders
    filameshio
    meshoptimizer
    camutils
    backend
    bluegl
    bluevk
    filabridge
    filaflat
    utils
    geometry
    smol-v
    ibl
    image
    vkshaders
    meshoptimizer
    assimp
    ktxreader
    pthread
    dl
    rt
    SDL2
    c++
    c++abi
    boost_filesystem
    )

# OpenSubdiv
find_path(OPENSUBDIV_PATH "OpenSubdiv")
set(CMAKE_LINK_SEARCH_START_STATIC 1)
target_link_directories(${CMAKE_PROJECT_NAME} PRIVATE ${OPENSUBDIV_PATH}/lib)
target_include_directories(${CMAKE_PROJECT_NAME}  PRIVATE ${OPENSUBDIV_PATH}/include)
target_link_libraries(${CMAKE_PROJECT_NAME}
    osdCPU
    osdGPU
    )


# paths for binaries and headers
install(TARGETS ${PROJECT_NAME}
    EXPORT "${PROJECT_NAME}Config"
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
    ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
    RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
    INCLUDES DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}
    )

install(
    EXPORT "${PROJECT_NAME}Config"
    FILE "${PROJECT_NAME}Config.cmake"
    NAMESPACE filasofia::
    DESTINATION cmake
    )
