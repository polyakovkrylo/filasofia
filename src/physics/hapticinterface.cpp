#include "physics/hapticinterface.h"
#include "physics/physicalbody.h"

#include <sofa/simulation/graph/SimpleApi.h>
#include <sofa/helper/system/PluginManager.h>

namespace filasofia {

using sofa::simpleapi::createObject;
using sofa::helper::system::PluginManager;

using std::to_string;
using std::stringstream;

using Eigen::Translation3d;
using Eigen::Quaterniond;

HapticInterface::HapticInterface(const std::string &name,
                                 sofa::simulation::Node::SPtr &parent) :
    Node(name, parent)
{

}

HapticInterface::Builder::Builder(const std::string &name) :
    m_name(name)
{

}

HapticInterface::Builder &HapticInterface::Builder::device(HapticInterface::Device device)
{
    m_device = device;
    return *this;
}

HapticInterface::Builder &HapticInterface::Builder::scale(float scale)
{
    m_scale = scale;
    return *this;
}

HapticInterface::Builder &HapticInterface::Builder::origin(const Translation3d &translation, const Quaterniond &rotation)
{
    m_translation = translation;
    m_rotation = rotation;
    return *this;
}

HapticInterface::Builder &HapticInterface::Builder::controlledBody(const std::shared_ptr<PhysicalBody> &body)
{
    m_controlledBody = body;
    return *this;
}

std::shared_ptr<HapticInterface> HapticInterface::Builder::build(sofa::simulation::Node::SPtr &parent)
{
    m_interface.reset(new HapticInterface(m_name, parent));

    std::string deviceName;
    std::string pluginName;
    std::string poseDataName;

    std::map<std::string, std::string> hiParams = {
        {"name", "HapticInterface"},
        {"scale", to_string(m_scale)}
    };

    stringstream translation_ss;
    translation_ss << m_translation.vector();

    stringstream orientation_ss;
    orientation_ss << m_rotation.x() << " "
                   << m_rotation.y() << " "
                   << m_rotation.z() << " "
                   << m_rotation.w();

    switch (m_device) {
    case Device::Omega:
        pluginName = "ForceDimensions";
        deviceName = "OmegaDriver";
        poseDataName = "pose";
        hiParams["baseFrame"] = translation_ss.str() + " " + orientation_ss.str();
        break;

    case Device::Geomagic:
        pluginName = "Geomagic";
        deviceName = "GeomagicDriver";
        poseDataName = "positionDevice";
        hiParams["positionBase"] = translation_ss.str();
        hiParams["orientationBase"] = orientation_ss.str();
        break;
    };

    if (!pluginName.empty())
        sofa::helper::system::PluginManager::getInstance().loadPlugin(pluginName);

    sofa::simpleapi::createObject(m_interface->node(), deviceName, hiParams);

    sofa::simpleapi::createObject(m_interface->node(), "MechanicalObject", {
                                      {"name", "DOF"},
                                      {"template", "Rigid3d"},
                                      {"position", "@HapticInterface." + poseDataName}
                                  });

    if (m_controlledBody)
        buildAttachment();

    return std::move(m_interface);
}

void HapticInterface::Builder::buildAttachment()
{
    // TODO: Remove magic numbers
    auto externalRestShapePath = "@" + m_interface->node()->getPathName() + "/DOF";

    sofa::simpleapi::createObject(m_controlledBody->node(), "RestShapeSpringsForceField", {
                                      {"stiffness", "1e9"},
                                      {"angularStiffness", "1e9"},
                                      {"external_rest_shape", externalRestShapePath},
                                      {"rayleighStiffness" ,"0.05"},
                                      {"points", "0"}
                                  });
}

}
