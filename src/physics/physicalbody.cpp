/**
 * @file The PhysicsBody class definition
 * @author Vladimir Poliakov
 */
#include "physics/physicalbody.h"
#include "common/utils.h"
#include <sofa/simulation/graph/SimpleApi.h>
#include <sofa/component/statecontainer/MechanicalObject.h>
#include <sofa/component/constraint/projective/AttachConstraint.h>

namespace filasofia {

using std::make_unique;
using std::string;
using std::to_string;
using std::vector;

using sofa::simpleapi::createChild;
using sofa::simpleapi::createObject;
using sofa::core::objectmodel::BaseObject;
typedef sofa::component::constraint::projective::AttachConstraint<sofa::defaulttype::Vec3Types> AttachConstraint;

using SofaNode = sofa::simulation::Node;

using filasofia::endsWith;

void PhysicalBody::init()
{
    std::vector<AttachConstraint*> attachments;
    node()->get<AttachConstraint>(&attachments, sofa::core::objectmodel::BaseContext::SearchDown);
    for (auto &a: attachments) {
        a->d_positionFactor.setValue(1.0);
        // required to process topological changes
        a->f_indices1.createTopologyHandler(a->getContext()->get<sofa::core::topology::BaseMeshTopology>());
        a->f_indices2.createTopologyHandler(a->getContext()->get<sofa::core::topology::BaseMeshTopology>());
    }
}

PhysicalBody::PhysicalBody(const std::string &name, sofa::simulation::Node::SPtr &parent) :
    Node(name, parent)
{
}

PhysicalBody::Builder::Builder(const std::string &bodyName) :
    m_bodyName(bodyName)
{

}

PhysicalBody::Builder &PhysicalBody::Builder::mechanicalModel(const PhysicalBody::MechanicalModelParams &params, const std::string &filename)
{
    m_mechanicalModel = filename;
    m_mechanicalModelParams = params;
    return *this;
}

PhysicalBody::Builder &PhysicalBody::Builder::attach(const std::shared_ptr<PhysicalBody> &body,
                                                     const AttachmentParams &params)
{
    m_attachments.push_back({body, params});
    return *this;
}

PhysicalBody::Builder &PhysicalBody::Builder::pinArea(const vector<PhysicalBody::Builder::AABB> &roi, const double &stiffness)
{
    m_pinnedAreas.push_back({roi, stiffness});
    return *this;
}

PhysicalBody::Builder &PhysicalBody::Builder::collisionModel(const PhysicalBody::CollisionModelParams &params, const std::string &filename)
{
    m_collisionModels[filename] = params;
    return *this;
}

PhysicalBody::Builder &PhysicalBody::Builder::map(const std::shared_ptr<PhysicalBody> &source, int index)
{
    m_mappedTo = { source, index };
    return *this;
}

PhysicalBody::Builder &PhysicalBody::Builder::translation(const Eigen::Vector3d &translation)
{
    m_translation = translation;
    return *this;
}

PhysicalBody::Builder &PhysicalBody::Builder::forceFeedback(float scale)
{
    m_forceFeedback = scale;
    return *this;
}

std::shared_ptr<PhysicalBody> PhysicalBody::Builder::build(sofa::simulation::Node::SPtr &parent)
{
    m_body.reset(new PhysicalBody(m_bodyName, parent));

    buildMechanicalModel();

    if (m_forceFeedback> 0.0) {
        createObject(m_body->node(), "LCPForceFeedback", {
                         {"activate", "true"},
                         {"forceCoef", to_string(m_forceFeedback)}
                     });
    }

    if (!m_collisionModels.empty())
        buildCollisionModels();

    if (!m_pinnedAreas.empty() || !m_attachments.empty())
        buildAttachments();

    return std::move(m_body);
}

void PhysicalBody::Builder::buildMechanicalModel()
{
    // create a solver
    const auto &solverParams = m_mechanicalModelParams.solverParams;
    if (solverParams.integrator != IntegratorType::None) {
        std::string ode = (solverParams.integrator == IntegratorType::Implicit)
                    ? "EulerImplicitSolver"
                    : "EulerExplicitSolver";

        createObject(m_body->node(), ode, {
                         {"name", "ODE Solver"},
                         {"rayleighStiffness", to_string(solverParams.rayleighStiffness)},
                         {"rayleighMass", to_string(solverParams.rayleighMass)}
                     });

        switch (solverParams.linearSolver) {
        case SolverType::Iterative:
            createObject(m_body->node(), "CGLinearSolver", {
                             {"name", "LinearSolver"},
                             {"iterations", to_string(solverParams.iterations)},
                             {"tolerance", to_string(solverParams.precision)},
                             {"threshold", to_string(solverParams.precision)}
                         });
            break;

        case SolverType::Direct:
            createObject(m_body->node(), "SparseCholesky", {
                             {"name", "LinearSolver"}
                         });
            break;

        default:
            break;
        }

    }

    m_body->m_type = m_mechanicalModelParams.type;
    switch (m_body->m_type) {
    case BodyType::SoftTetrahedronFem:
    case BodyType::SoftMassSpring:
        createMeshLoader(m_body->node(), m_mechanicalModel);

        m_body->m_dynamicTopology = m_mechanicalModelParams.dynamicTopology;

        m_body->m_mechanicalMesh = createObject(m_body->node(), "TetrahedronSetTopologyContainer", {
                         {"name", "TopologyContainer"},
                         {"src", "@MeshLoader"}
                     });

        createObject(m_body->node(), "TetrahedronSetGeometryAlgorithms", {
                         {"name", "GeometryAlgorithms"},
                         {"template", "Vec3d"}
                     });

        createObject(m_body->node(), "TetrahedronSetTopologyModifier");

        createObject(m_body->node(), "MechanicalObject", {
                         {"name", "DOF"},
                         {"template", "Vec3d"},
                         {"translation", toString(m_translation)},
                         {"src", "@MeshLoader"}
                     });

        if (m_body->m_dynamicTopology) {
            createObject(m_body->node(), "LineCollisionModel", {
                             {"name", "CuttingCollisionModel"},
                             {"proximity", "0"},
                             {"domain", "1"},
                             {"contactResponse", "CuttingContactConstraint"}
                         });
            createObject(m_body->node(), "CuttingProcessManager", {{"snappingDistance", "0.1"}});
            createObject(m_body->node(), "TearingManager", {
                             {"tearingThreshold", toString(m_mechanicalModelParams.tearingThreshold)}
                         });
        }
    {
        const auto &bodyParams = m_mechanicalModelParams.softBodyParams;
        if(m_mechanicalModelParams.type == BodyType::SoftTetrahedronFem) {
            createObject(m_body->node(), "TetrahedronFEMForceField",
                         {
                             {"youngModulus", to_string(bodyParams.youngModulus)},
                             {"poissonRatio", to_string(bodyParams.poissonRatio)}
                         });
        } else {
            createObject(m_body->node(), "MeshSpringForceField",
                         {
                             {"tetrasStiffness", to_string(bodyParams.stiffness)},
                             {"tetrasDamping", to_string(bodyParams.damping)}
                         });
        }
    }

        break;

    case BodyType::Rigid:
        createObject(m_body->node(), "MechanicalObject", {
                         {"name", "DOF"},
                         {"template", "Rigid3d"},
                         {"translation", toString(m_translation)},
                         {"size", to_string(m_mechanicalModelParams.numberOfElements)}
                     });
        break;
    }

    if (m_mechanicalModelParams.mass
            && (m_mechanicalModelParams.solverParams.linearSolver != SolverType::None)) {

        switch(m_mechanicalModelParams.massType) {
        case MassType::Uniform:
            createObject(m_body->node(), "UniformMass", {
                             {"name", "Mass"},
                             {"totalMass", to_string(m_mechanicalModelParams.mass)}
                         });
            break;
        case MassType::Diagonal:
            createObject(m_body->node(), "DiagonalMass", {
                             {"name", "Mass"},
                             {"totalMass", to_string(m_mechanicalModelParams.mass)}
                         });
            break;
        }

        switch (solverParams.correction) {
        case PhysicalBody::ConstraintCorrection::Uncoupled:
            createObject(m_body->node(), "UncoupledConstraintCorrection", {
                             {"name", "ConstraintCorrection"},
                             {"compliance", to_string(solverParams.compliance) }
                         });
            break;
        case PhysicalBody::ConstraintCorrection::Precomputed:
            createObject(m_body->node(), "PrecomputedConstraintCorrection", {
                             {"name", "ConstraintCorrection"},
                             {"fileDir", "."}
                         });
            break;
        }
    }

    if (m_mappedTo.first) {
        std::map<std::string, std::string> mappingParams = {
            {"name", "Mapping"},
            {"input", "@" + m_mappedTo.first->node()->getPathName() + "/DOF"},
            {"output", "@DOF" }
        };
        switch(m_mappedTo.first->type()) {
        case BodyType::Rigid:
            mappingParams["index"] = to_string(m_mappedTo.second);
            createObject(m_body->node(),
                         (m_body->m_type == BodyType::Rigid)
                            ? "RigidRigidMapping"
                            : "RigidMapping",
                         mappingParams);
            break;
        case BodyType::SoftTetrahedronFem:
        case BodyType::SoftMassSpring:
            createObject(m_body->node(), "BarycentricMapping", mappingParams);
        default: break;
        }
    }

}

void PhysicalBody::Builder::buildCollisionModels()
{
    for (auto &[mesh, params] : m_collisionModels) {
        m_body->m_collisionModel = createChild(m_body->node(), "CollisionModel");

        if (m_mechanicalModelParams.dynamicTopology) {
            createObject(m_body->m_collisionModel, "TriangleSetTopologyContainer");
            createObject(m_body->m_collisionModel, "TriangleSetTopologyModifier");
            createObject(m_body->m_collisionModel, "Tetra2TriangleTopologicalMapping");
        }
        else {
            createMeshLoader(m_body->m_collisionModel, mesh);
            m_body->m_collisionMesh = createObject(m_body->m_collisionModel, "MeshTopology",{
                                                       {"name", "Topology"},
                                                       {"src", "@MeshLoader"}
                                                   });

            createObject(m_body->m_collisionModel, "MechanicalObject", {
                             {"name", "DOF"},
                             {"template", "Vec3d"}
                         });

            if (m_mechanicalModelParams.type == PhysicalBody::BodyType::Rigid) {
                createObject(m_body->m_collisionModel, "RigidMapping", {
                                 {"name", "Mapping"},
                                 {"input", "@.."},
                                 {"mapForces", "false"}
                             });
            }
            else {
                createObject(m_body->m_collisionModel, "BarycentricMapping", {
                                 {"name", "Mapping"},
                                 {"input", "@.."},
                                 {"mapForces", "false"}
                             });
            }
        }

        auto proxStr = to_string(params.proximity);
        auto groupStr = to_string(params.group);
        auto selfCollisionStr = to_string(params.selfCollision);

        if (params.cutting) {
            if (m_mechanicalModelParams.type != PhysicalBody::BodyType::Rigid)
                throw std::runtime_error("Only rigid objects can have cutting collision models");

            createObject(m_body->m_collisionModel, "LineCollisionModel",{
                             { "name", "LineModel" },
                             { "proximity", proxStr },
                             { "group", groupStr },
                             { "domain", "1" },
                             { "contactResponse", "CuttingContactConstraint" }
                         });

            createObject(m_body->node(), "CuttingTool");
        }
        else {
            if (params.primitives & CollisionModelFlags::Point)
                createObject(m_body->m_collisionModel, "PointCollisionModel",{
                                 { "name", "PointModel" },
                                 { "proximity", proxStr },
                                 { "selfCollision", selfCollisionStr},
                                 { "group", groupStr }
                             });

            if (params.primitives & CollisionModelFlags::Line)
                createObject(m_body->m_collisionModel, "LineCollisionModel",{
                                 { "name", "LineModel" },
                                 { "proximity", proxStr },
                                 { "selfCollision", selfCollisionStr},
                                 { "group", groupStr }
                             });

            if (params.primitives & CollisionModelFlags::Triangle)
                createObject(m_body->m_collisionModel, "TriangleCollisionModel",{
                                 { "name", "TriangleModel" },
                                 { "proximity", proxStr },
                                 { "selfCollision", selfCollisionStr},
                                 { "group", groupStr }
                             });
        }
    }
}

void PhysicalBody::Builder::buildAttachments()
{
    for(size_t i = 0; i < m_pinnedAreas.size(); i++) {
        const auto &roi = m_pinnedAreas[i].first;
        const float &stiffness = m_pinnedAreas[i].second;

        string roiStr;
        for (auto &b : roi) for (auto &v: b){
            roiStr += to_string(v) + " ";
        }

        auto box = createObject(m_body->node(), "BoxROI", {
                                    {"name", "Box" + std::to_string(i)},
                                    {"box", roiStr}
                                });

        if (stiffness < 0.0) {
            createObject(m_body->node(), "FixedConstraint",
                         {
                             {"name", "FixedConstraint" + std::to_string(i)},
                             {"indices", "@" + string(box->name.getValue()) + ".indices"}
                         });
        } else {
            createObject(m_body->node(), "RestShapeSpringsForceField",
                         {
                             {"name", "RestShapeConstraint" + std::to_string(i)},
                             {"stiffness", to_string(stiffness)},
                             {"rayleighStiffness","0.05"},
                             {"points", "@" + string(box->name.getValue()) + ".indices"}
                         });
        }
    }

    for (auto &[body, params] : m_attachments) {
        std::string name = "Attachment_" + body->name();
        std::string bodySrc = "@" + body->node()->getMechanicalState()->getPathName();

        if (params.barycentric) {
            auto attachmentNode = createChild(m_body->node(), "BarycentricAttachment");
            params.indicesFrom = params.indicesTo;
            createObject(attachmentNode, "PointSetTopologyContainer",{{"name", "Topology"}});
            createObject(attachmentNode, "PointSetTopologyModifier");
            createObject(attachmentNode, "MechanicalObject", {
                             {"name", "DOF"},
                             {"template", "Vec3d"},
                             {"position", "@../MeshLoader.position"}
                         });
            createObject(attachmentNode, "Mesh2PointTopologicalMapping");
            createObject(attachmentNode, "BarycentricMapping", {
                            {"input", bodySrc}
                         });
            bodySrc = "@./BarycentricAttachment/DOF";
        }

        std::ostringstream indicesSrc, indicesDst;
        std::copy(params.indicesFrom.begin(), params.indicesFrom.end(), std::ostream_iterator<int>(indicesSrc, " "));
        std::copy(params.indicesTo.begin(), params.indicesTo.end(), std::ostream_iterator<int>(indicesDst, " "));

        if (params.linearStiffness > 0) {
            createObject(m_body->node(), "RestShapeSpringsForceField", {
                             {"name", name},
                             {"external_rest_shape", bodySrc},
                             {"points", indicesDst.str()},
                             {"external_points", indicesSrc.str()},
                             {"linearStiffness", to_string(params.linearStiffness)},
                             {"angularStiffness", to_string((params.angularStiffness > 0)
                              ? params.angularStiffness
                              : params.linearStiffness)}
                         });
        }
        else {
            createObject(m_body->node(), "AttachConstraint", {
                             {"name", name},
                             {"object2", "@./DOF"},
                             {"object1", bodySrc},
                             {"positionFactor", "0"},
                             {"indices2", indicesDst.str()},
                             {"indices1", indicesSrc.str()},
                         });
        }
    }
}

BaseObject::SPtr PhysicalBody::Builder::createMeshLoader(const SofaNode::SPtr &parent, const std::string &filename)
{
    string type;

    if (endsWith(filename, ".obj"))
        type = "MeshObjLoader";
    else if (endsWith(filename, ".vtu"))
        type = "MeshVTKLoader";
    else if (endsWith(filename, ".stl"))
        type = "MeshSTLLoader";
    else if (endsWith(filename, ".msh"))
        type = "MeshGmshLoader";

    if (type.empty())
        throw std::runtime_error("wrong file extension");

    return createObject(parent, type, {
                     { "name", "MeshLoader" },
                     { "filename", filename }
                 });
}

}
