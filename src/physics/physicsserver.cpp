/**
 * @file The PhysicsServer class definition
 * @author Vladimir Poliakov
 */
#include "physics/physicsserver.h"

#include "simulation/event/eventdispatcher.h"

#include <sofa/simulation/graph/init.h>
#include <sofa/simulation/graph/SimpleApi.h>
#include <sofa/component/init.h>

#include <sofa/helper/system/PluginManager.h>

#include <cstdlib>

namespace filasofia {

using namespace sofa;

using std::chrono::microseconds;
using std::string;
using std::shared_mutex;
using std::make_unique;
using std::shared_ptr;
using std::unique_ptr;

PhysicsServer::PhysicsServer(const shared_ptr<EventDispatcher> &dispatcher,
        const microseconds & period, const microseconds &slack) :
    PeriodicTask(period, true),
    m_eventDispatcher(dispatcher),
    m_slack(slack),
    m_mutex(make_unique<shared_mutex>())
{
    component::init();

    simulation::graph::init();

    m_simulation = simpleapi::createSimulation();
    simulation::setSimulation(m_simulation.get());
    m_rootNode= simpleapi::createRootNode(m_simulation, "Root");

    std::vector<std::string> requiredPlugins = {
        "ArticulatedSystemPlugin",
        "InteractiveCutting",
        "Tearing",
        "MultiThreading",
    };

    for (auto &p : requiredPlugins)
        sofa::helper::system::PluginManager::getInstance().loadPlugin(p);
}

void PhysicsServer::init()
{
    unique_ptr<Event> ev = make_unique<Event>(Event::Type::PhysicsInit);
    m_eventDispatcher->post(ev);

    for (auto &plugin : m_plugins) {
        sofa::helper::system::PluginManager::getInstance().loadPlugin(plugin);
    }

    sofa::simpleapi::createObject(m_rootNode, "LocalMinDistance", {
                                      {"alarmDistance", "0.06"},
                                      {"contactDistance", "0.04"}
                                  });

    sofa::simpleapi::createObject(m_rootNode, "CollisionPipeline", {
                                      {"depth", "6"},
                                      {"verbose", "0"}
      });
    sofa::simpleapi::createObject(m_rootNode, "ParallelBruteForceBroadPhase");
    sofa::simpleapi::createObject(m_rootNode, "ParallelBVHNarrowPhase");

    for (auto &obj : m_rootNodeComponents) {
        simpleapi::createObject(m_rootNode, obj.first, obj.second);
    }

    m_simulation->init(m_rootNode.get());

    PeriodicTask::init();
}

void PhysicsServer::step(std::chrono::microseconds dt)
{
    unique_ptr<Event> ev = make_unique<Event>(Event::Type::PhysicsUpdate);
    m_eventDispatcher->post(ev);

    m_mutex->lock();

    // saturating to the maximum allowed dt
    std::chrono::duration<double, std::chrono::seconds::period> timestep = std::min(dt, period() + m_slack);
    m_simulation->animate(m_rootNode.get(), timestep.count());

    m_mutex->unlock();
}

void PhysicsServer::cleanup()
{
    unique_ptr<Event> ev = make_unique<Event>(Event::Type::PhysicsCleanup);
    m_eventDispatcher->post(ev);
    simulation::graph::cleanup();
    PeriodicTask::cleanup();
}

void PhysicsServer::removePlugin(const string &name)
{
    m_plugins.erase(m_plugins.find(name));
}

void PhysicsServer::addRootNodeComponent(const string &name, const std::map<string, string> &parameters)
{
    m_rootNodeComponents[name] = parameters;
}

void PhysicsServer::addDataRepository(const FileRepository &repo)
{
    populateRepository(repo, sofa::helper::system::DataRepository);
}

void PhysicsServer::addPluginRepository(const FileRepository &repo)
{
    populateRepository(repo, sofa::helper::system::PluginRepository);
}

const shared_ptr<PhysicalBody> PhysicsServer::addBody(PhysicalBody::Builder &builder)
{
    m_bodies.push_back(builder.build(m_rootNode));
    return m_bodies.back();
}

const shared_ptr<HapticInterface> & PhysicsServer::addHapticInterface(HapticInterface::Builder &builder)
{
    if (isInitialized())
        throw std::runtime_error("Haptic interface can only be set before init() is called!");

    m_hapticInterfaces.push_back(builder.build(m_rootNode));
    return m_hapticInterfaces.back();
}

const std::shared_ptr<Articulation> &PhysicsServer::addArticulation(Articulation::Builder &builder)
{
    if (isInitialized())
        throw std::runtime_error("Articulation can only be set before init() is called!");

    m_articulations.push_back(builder.build(m_rootNode));
    return m_articulations.back();
}

void PhysicsServer::initBodies()
{
    for (auto &body : m_bodies) {
        body->init();
    }
}

void PhysicsServer::setCollisionRule(const std::pair<int, int> collisionGroups, const std::string &response, const std::vector<std::pair<std::string, std::string> > &params)
{
    // Build the rule in accordance with the SOFA syntax:
    // "[group1] [group2] [Response]?[param1=val]&?[param2=val][..]"

    // add group indices and the response type
    std::ostringstream rule;
    for (auto &g: {collisionGroups.first, collisionGroups.second}) {
        rule << ((g<0) ? "*" : std::to_string(g)) << " ";
    }
    rule << response;

    // add each parameter
    bool firstElement = true;
    for (auto &p: params) {
        rule << ((firstElement)?"?":"&?");
        rule << p.first << "=" << p.second;
        firstElement = false;
    }

    // add the rule to the contact manager's list of rules
    m_rootNodeComponents["RuleBasedContactManager"]["rules"].append(" " + rule.str());
}

void PhysicsServer::populateRepository(const FileRepository &source, helper::system::FileRepository &destination)
{
    for (const auto &path : source.paths()) {
        destination.addLastPath(path.native());
    }
}

void PhysicsServer::setSlack(std::chrono::microseconds slack)
{
    m_slack = slack;
}

void PhysicsServer::setSimulationType(PhysicsServer::SimulationType type)
{
    if (isInitialized())
        throw std::runtime_error("Simulation type can only be set before init() is called!");

    // delete all components related to the simulation type
    m_rootNodeComponents.erase("FreeMotionAnimationLoop");
    m_rootNodeComponents.erase("CollisionResponse");
    m_rootNodeComponents.erase("GenericConstraintSolver");

    switch (type) {
    case SimulationType::FreeMotionAnimation:
        addRootNodeComponent("FreeMotionAnimationLoop", {
                                 {"name", "AnimationLoop"},
                                 {"parallelCollisionDetectionAndFreeMotion", "true"},
                                 {"parallelODESolving", "true"}
                             });

        addRootNodeComponent("RuleBasedContactManager", {
                                 {"name", "Response"},
                                 {"response", "FrictionContactConstraint"},
                                 {"responseParams", "mu=1e-3"}
                             });

        addRootNodeComponent("GenericConstraintSolver", {
                                 {"tolerance", "1e-6"},
                                 {"maxIt", "1000"},
                                 {"multithreading", "true"},
                             });
        break;
    case SimulationType::Default:
        addRootNodeComponent("CollisionResponse", {
                                 {"name", "Response"},
                                 {"response", "Default"}
                             });
        break;
    }
}

void PhysicsServer::setGravity(const Eigen::Vector3d &gravity)
{
    m_rootNode->setGravity(sofa::type::Vec3(gravity.x(), gravity.y(), gravity.z()));
}

}
