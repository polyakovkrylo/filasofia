#include "physics/articulation.h"
#include "physics/physicalbody.h"
#include "common/utils.h"

#include <sofa/simulation/graph/SimpleApi.h>

namespace filasofia {

using std::string;
using std::to_string;
using sofa::simpleapi::createChild;
using sofa::simpleapi::createObject;
using sofa::core::objectmodel::New;

Articulation::Articulation(const string &name, sofa::simulation::Node::SPtr &parent) :
    Node(name, parent)
{

}

Articulation::Builder::Builder(const std::string &name) :
    m_name(name)
{

}

Articulation::Builder &Articulation::Builder::joint(JointConfiguration &joint)
{
    // the default parent index is the last index in the previous
    // all previous joints
    if(joint.parentIndex == -1)
        joint.parentIndex = m_joints.size();
    if(joint.childIndex == -1)
        joint.childIndex = m_joints.size() + 1;
    m_joints.push_back(joint);
    return *this;
}

Articulation::Builder &Articulation::Builder::attachedTo(const std::shared_ptr<PhysicalBody> &body)
{
    m_attachment = body;
    return *this;
}

Articulation::Builder &Articulation::Builder::controlledBody(const std::shared_ptr<PhysicalBody> &body)
{
    m_controlledBody = body;
    return *this;
}

std::shared_ptr<Articulation> Articulation::Builder::build(sofa::simulation::Node::SPtr &parent)
{
    m_articulation.reset(new Articulation(m_name, parent));

    createObject(m_articulation->node(), "MechanicalObject", {
                     {"name", "DOF"},
                     {"size", to_string(m_joints.size())},
                     {"template", "Vec1d"}
                 });

    createObject(m_articulation->node(), "EulerImplicitSolver", {
                     {"raileighMass", "0.5"},
                     {"raileighStiffness", "0.1"}
                 });
    createObject(m_articulation->node(), "CGLinearSolver", {
                     {"iterations", "25"},
                     {"tolerance", "1e-6"},
                     {"threshold", "1e-6"}
                 });

    string pointsStr;
    string stiffnessStr;
    string complianceStr;
    for (uint i = 0; i < m_joints.size(); ++i) {
        pointsStr += to_string(i) + " ";
        stiffnessStr += to_string(m_joints[i].stiffness) + " ";
        complianceStr += to_string(m_joints[i].contactCompliance) + " ";
    }

    createObject(m_articulation->node(), "RestShapeSpringsForceField", {
                     {"name", "SpringField"},
                     {"stiffness", stiffnessStr},
                     {"points", pointsStr}
                 });
    createObject(m_articulation->node(), "UncoupledConstraintCorrection", {
                     {"compliance", complianceStr }
                 });

    if (m_controlledBody) {
        std::map<std::string, std::string> articulatedSystemParams = {
            {"input1", "@" + m_articulation->node()->getPathName() + "/DOF"},
            {"output", "@DOF"}
        };

        if (m_attachment)
            articulatedSystemParams["input2"] = "@" + m_attachment->node()->getPathName() + "/DOF";

        createObject(m_controlledBody->node(), "ArticulatedSystemMapping", articulatedSystemParams);
    }

    createObject(m_articulation->node(), "ArticulatedHierarchyContainer");

    auto graph = createChild(m_articulation->node(), "ArticulationGraph");

    for (size_t i = 0; i < m_joints.size(); i++) {
        // create the articulation the articulation center
        auto &joint = m_joints[i];

        auto jointNode = createChild(graph, "Joint" + to_string(i+1));
        createObject(jointNode, "ArticulationCenter", {
                         {"parentIndex", to_string(joint.parentIndex)},
                         {"childIndex", to_string(joint.childIndex)},
                         {"posOnParent", toString(joint.parentOffset)},
                         {"posOnChild", toString(joint.childOffset)},
                         {"articulationProcess", to_string(joint.process)}
                     });
        auto articulationNode = createChild(jointNode, "Articulation");
        createObject(articulationNode, "Articulation", {
                         {"translation", to_string(joint.type == JointType::Prismatic)},
                         {"rotation", to_string(joint.type == JointType::Revolute)},
                         {"axis", toString(joint.axis)},
                         {"articulationIndex", to_string(i)}
                     });
    }

    return std::move(m_articulation);
}

}
