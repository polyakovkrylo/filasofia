/**
 * @file Node class definition
 * @author Vladimir Poliakov
 */
#include "physics/node.h"

#include <sofa/simulation/graph/SimpleApi.h>

namespace filasofia {

using sofa::simpleapi::createChild;

Node::Node(const std::string &name, sofa::simulation::Node::SPtr & parent) :
    m_name(name)
{
    m_node = createChild(parent, name);
}

}
