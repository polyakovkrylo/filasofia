#include "simulation/window/window.h"

#include "simulation/event/keyevent.h"
#include "simulation/event/mouseevent.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_syswm.h>

namespace filasofia::window {

using std::unique_ptr;
using std::make_unique;

AbstractWindow::AbstractWindow(const std::string &name, const Geometry &geometry) :
    m_name(name),
    m_geometry(geometry)
{

}

SDLWindow::SDLWindow(const std::string &name, const Geometry &geometry) :
    AbstractWindow (name, geometry)
{

}

SDLWindow::~SDLWindow()
{
    SDL_Quit();
}

void SDLWindow::init()
{
    /* Initialize the library */
    SDL_Init(SDL_INIT_EVERYTHING);
    m_sdlWindow.reset(SDL_CreateWindow(
                name().data(),
                geometry().position.x(),
                geometry().position.y(),
                geometry().size.x(),
                geometry().size.y(),
                SDL_WINDOW_VULKAN
                ));
}

void * SDLWindow::nativeWindow()
{
    SDL_SysWMinfo wmi;
    SDL_VERSION(&wmi.version);
    SDL_GetWindowWMInfo(m_sdlWindow.get(), &wmi);
    return reinterpret_cast<void *>(wmi.info.x11.window);
}

std::unique_ptr<Event> SDLWindow::poll()
{
    unique_ptr<Event> ev;

    SDL_Event event;
    SDL_PollEvent(&event);

    switch (event.type) {
    case SDL_QUIT:
        ev = make_unique<Event>(Event::Type::Quit);
        break;

    case SDL_KEYDOWN:
    case SDL_KEYUP: {
        auto modifiers = mapModifiers(event.key.keysym.mod);

        Event::Type type = (event.type == SDL_KEYDOWN)
                ? Event::Type::KeyPressed
                : Event::Type::KeyReleased;

        uint16_t key = event.key.keysym.scancode;

        ev = make_unique<KeyEvent>(type, key, (type == Event::Type::KeyPressed), modifiers);

        break;
    }
    case SDL_MOUSEBUTTONDOWN:
    case SDL_MOUSEBUTTONUP:
    {
        auto modifiers = mapModifiers(event.key.keysym.mod);

        Event::Type type = (event.type == SDL_MOUSEBUTTONDOWN)
                  ? Event::Type::MousePressEvent
                  : Event::Type::MouseReleaseEvent;

        MouseEvent::ButtonFlags button;
        switch (event.button.button) {
        case SDL_BUTTON_LEFT:
            button = MouseEvent::Left;
            break;
        case SDL_BUTTON_RIGHT:
            button = MouseEvent::Right;
            break;
        case SDL_BUTTON_MIDDLE:
            button = MouseEvent::Middle;
            break;
        }

        MouseEvent::Point pos(event.button.x, event.button.y);

        ev = make_unique<MouseEvent>(type, button, pos, modifiers);

        break;
    }
    default:
        ev = make_unique<Event>(Event::Type::Empty);
        break;
    }

    return ev;
}

ModifierKeyFlags SDLWindow::mapModifiers(uint16_t sdl_modifiers)
{
    ModifierKeyFlags modifiers = 0;

    auto map = [&](ModifierKey out, uint16_t in) {
        if (sdl_modifiers & in)
            modifiers |= out;
    };

    map(ModifierKey::Alt, KMOD_ALT);
    map(ModifierKey::Control, KMOD_CTRL);
    map(ModifierKey::Shift, KMOD_SHIFT);
    map(ModifierKey::KeyPad, KMOD_NUM);
    map(ModifierKey::Meta, KMOD_GUI);

    return modifiers;
}

void SDLWindowDestroyer::operator()(SDL_Window *w) const
{
    SDL_DestroyWindow(w);
}

}
