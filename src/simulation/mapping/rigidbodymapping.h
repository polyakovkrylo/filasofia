/**
 * @file The RigidBodyMapping class declaration
 * @author Vladimir Poliakov
 */
#pragma once

#include "simulation/mapping/abstractmapping.h"

namespace filasofia {

class RigidBodyMapping : public AbstractMapping
{
public:
    RigidBodyMapping(const std::shared_ptr<PhysicalBody> &physicsBody,
                     const std::shared_ptr<Transform> &visualEntity,
                     const std::shared_ptr<std::shared_mutex> &mutex);

    virtual ~RigidBodyMapping() override = default;

    virtual void update() override;
    virtual void init() override {}

private:
    const std::weak_ptr<Transform> m_visualEntity;
};

}
