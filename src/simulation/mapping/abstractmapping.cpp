/**
 * @file The AbstractMapping class definition
 * @author Vladimir Poliakov
 */
#include "simulation/mapping/abstractmapping.h"

namespace filasofia {

AbstractMapping::AbstractMapping(const std::shared_ptr<PhysicalBody> &physicsBody,
                                 const std::shared_ptr<std::shared_mutex> &mutex) :
    m_physicsBody(physicsBody),
    m_mutex(mutex)
{

}

}
