/**
 * @file The SoftBodyMapping class definition
 * @author Vladimir Poliakov
 */
#include "softbodymapping.h"

#include <sofa/component/mapping/linear/BarycentricMapping.h>

namespace filasofia {

using sofa::component::visual::VisualModelImpl;
using sofa::defaulttype::Vec3Types;
using Mapping = sofa::core::Mapping<Vec3Types, Vec3Types>;
using BarycentricMapping = sofa::component::mapping::linear::BarycentricMapping<Vec3Types, Vec3Types>;
using sofa::core::objectmodel::New;
using sofa::helper::WriteAccessor;
using sofa::core::objectmodel::Data;
using sofa::core::VecId;

SoftBodyMapping::SoftBodyMapping(const std::shared_ptr<PhysicalBody> &physicsBody,
                                 const std::shared_ptr<VisualBody> &visualBody,
                                 const std::shared_ptr<std::shared_mutex> &mutex) :
    AbstractMapping(physicsBody, mutex),
    m_visualBody(visualBody)
{
    createProxyModel();
}

void SoftBodyMapping::update()
{
    auto visualBody = m_visualBody.lock();

    if(!visualBody) {
        return;
    }

    m_mutex->lock_shared();
    m_mapping->apply();
    m_mutex->unlock_shared();

    auto &visualBodyMesh = visualBody->acquireMesh();


    auto &proxyPosition = m_visualProxy->getVertices();

#pragma omp parallel for
    for (unsigned int i = 0; i < proxyPosition.size(); i++) {
        visualBodyMesh.setVertexPosition(i, {proxyPosition[i][0],proxyPosition[i][1],proxyPosition[i][2]});
    }

    visualBody->submitMesh();
}

void SoftBodyMapping::createProxyModel()
{
    auto body = m_physicsBody.lock();

    m_visualProxy = New<VisualModelImpl>();
    populateProxyModel();

    BarycentricMapping::SPtr mapping = New<BarycentricMapping>();

    auto mechanicalState = reinterpret_cast<sofa::core::behavior::MechanicalState<sofa::defaulttype::Vec3Types> *>(body->node()->getMechanicalState());
    mapping->setModels(mechanicalState, m_visualProxy.get());
    mapping->init();

    m_mapping = mapping;
}

void SoftBodyMapping::populateProxyModel()
{    
    auto body = m_visualBody.lock();
    auto &mesh = body->mesh();

    WriteAccessor<Data<VisualModelImpl::VecCoord>> proxyPos = *m_visualProxy->write(VecId::position());
    proxyPos.resize(mesh.vertices().size());

    for (unsigned int i = 0; i < proxyPos.size(); i++) {
        auto pos = mesh.vertices().at(i).position;
        proxyPos[i] = {pos.x , pos.y, pos.z};
    }
}

}
