/**
 * @file The SoftBodyMapping class declaration
 * @author Vladimir Poliakov
 */
#pragma once

#include <sofa/component/visual/VisualModelImpl.h>
#include <sofa/core/BaseMapping.h>

#include "simulation/mapping/abstractmapping.h"

namespace filasofia {

class SoftBodyMapping : public AbstractMapping
{
public:
    SoftBodyMapping(const std::shared_ptr<PhysicalBody> &physicsBody,
                    const std::shared_ptr<VisualBody> &visualBody,
                    const std::shared_ptr<std::shared_mutex> &mutex);
    virtual ~SoftBodyMapping() override = default;

    virtual void update() override;
    virtual void init() override {}

protected:
    const std::weak_ptr<VisualBody> m_visualBody;

     // Proxy model is used to apply SOFA Barycentric mapping
    sofa::component::visual::VisualModelImpl::SPtr m_visualProxy;
    sofa::core::BaseMapping::SPtr m_mapping;

private:
    void createProxyModel();
    void populateProxyModel();
};

}
