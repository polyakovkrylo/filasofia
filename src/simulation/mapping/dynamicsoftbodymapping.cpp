#include "dynamicsoftbodymapping.h"

#include <sofa/core/topology/TopologyHandler.h>
#include <sofa/simulation/graph/SimpleApi.h>
#include <sofa/core/behavior/BaseMechanicalState.h>
#include <sofa/core/topology/TopologyData.inl>
#include <sofa/core/topology/TopologyChange.h>

using sofa::simpleapi::createChild;
using sofa::simpleapi::createObject;
using sofa::core::behavior::BaseMechanicalState;
using sofa::component::topology::container::dynamic::TriangleSetTopologyContainer;
using sofa::component::topology::container::dynamic::TriangleSetTopologyModifier;
using sofa::component::topology::container::dynamic::TriangleSetGeometryAlgorithms;
using sofa::defaulttype::Vec3Types;

using sofa::core::topology::TopologyChangeType;
using sofa::core::topology::TopologyChange;
using sofa::core::topology::TrianglesRemoved;
using sofa::core::topology::TrianglesAdded;
using sofa::core::topology::PointsRemoved;
using sofa::core::topology::PointsAdded;

namespace filasofia {

DynamicSoftBodyMapping::DynamicSoftBodyMapping(const std::shared_ptr<filasofia::PhysicalBody> &physicsBody,
                                               const std::shared_ptr<filasofia::VisualBody> &visualBody,
                                               const std::shared_ptr<std::shared_mutex> &mutex) :
    AbstractMapping(physicsBody, mutex),
    m_visualBody(visualBody),
    m_proxyTriangles(initData(&m_proxyTriangles, "Proxy triangles", "Triangles of the proxy model")),
    m_proxyPositions(initData(&m_proxyPositions, "Proxy positions", "Vertex positions of the proxy model"))
{
    auto surface = createChild(physicsBody->node(),"Surface");
    m_topologyContainer = boost::dynamic_pointer_cast<TriangleSetTopologyContainer>(
                createObject(surface, "TriangleSetTopologyContainer", {{"name", "SurfaceTopologyContainer"}})
                );
    m_topologyModifier = boost::dynamic_pointer_cast<TriangleSetTopologyModifier>(
                createObject(surface, "TriangleSetTopologyModifier")
                );
    m_geometryAlgorithms = boost::dynamic_pointer_cast<TriangleSetGeometryAlgorithms<Vec3Types>>(
            createObject(surface, "TriangleSetGeometryAlgorithms")
            );

    createObject(surface, "Tetra2TriangleTopologicalMapping", {
                     {"input", "@../TopologyContainer"},
                     {"output", "@./SurfaceTopologyContainer"},
//                     {"noNewTriangles", "1"}
                 });
}

void DynamicSoftBodyMapping::init()
{
    auto visualBody = m_visualBody.lock();

    if(!visualBody)
        throw std::runtime_error("Cannot acquire the visual body!");

    auto &visualBodyMesh = visualBody->acquireMesh();
    initProxyModel();
    syncTopology(visualBodyMesh);
    visualBody->submitMesh();
}

void DynamicSoftBodyMapping::update()
{
    auto visualBody = m_visualBody.lock();

    if(!visualBody)
        throw std::runtime_error("Cannot acquire the visual body!");

    auto &visualBodyMesh = visualBody->acquireMesh();

    if (m_topologyUpdateRequired) {
        syncTriangles(visualBodyMesh.indices());
        m_topologyUpdateRequired = false;
    }

    updateProxyPositions();

#pragma omp parallel for
    for (uint16_t i = 0; i < visualBodyMesh.vertices().size(); i++) {
        const auto &proxyPos = m_proxyPositions.getValue()[m_visualBodyToProxyIndices[i]];
        visualBodyMesh.setVertexPosition(i, { proxyPos[0],
                                              proxyPos[1],
                                              proxyPos[2]
                                         });
    }

    visualBody->submitMesh();
}

void DynamicSoftBodyMapping::initProxyModel()
{
    ProxyPositionVector &proxyPos = *(m_proxyPositions.beginEdit());
    proxyPos.resize(m_topologyContainer->getNbPoints());

    #pragma omp parallel for
    for (size_t i = 0; i < proxyPos.size(); ++i) {
        proxyPos[i] = m_geometryAlgorithms->getPointRestPosition(i);
    }

    m_proxyPositions.endEdit();

    ProxyTriangleVector &proxyTriangles = *(m_proxyTriangles.beginEdit());
    proxyTriangles.resize(m_topologyContainer->getNumberOfTriangles());

#pragma omp parallel for
    for (size_t i = 0; i < proxyTriangles.size(); ++i) {
        proxyTriangles[i] = m_topologyContainer->getTriangle(i);
    }
    m_proxyTriangles.endEdit();

    m_proxyTriangles.createTopologyHandler(m_topologyContainer.get());
    m_proxyPositions.createTopologyHandler(m_topologyContainer.get());

    createTopologyChangeCallbacks();
}

void DynamicSoftBodyMapping::updateProxyPositions()
{
    m_mutex->lock_shared();
    ProxyPositionVector &proxyPos = *(m_proxyPositions.beginEdit());

#pragma omp parallel for
    for (size_t i = 0; i < proxyPos.size(); ++i) {
        proxyPos[i] = m_geometryAlgorithms->getPointPosition(i);
    }
    m_proxyPositions.endEdit();
    m_mutex->unlock_shared();
}

void DynamicSoftBodyMapping::syncTopology(const VisualMesh &visualMesh)
{
    syncVertices(visualMesh.vertices());
    syncTriangles(visualMesh.indices());
}

void DynamicSoftBodyMapping::syncVertices(const std::vector<VertexData> &visualBodyVertices)
{
    m_visualBodyToProxyIndices.clear();
    m_visualBodyToProxyIndices.resize(visualBodyVertices.size());
    m_proxyToVisualBodyIndices.clear();
    m_proxyToVisualBodyIndices.resize(m_topologyContainer->getNbPoints());
    std::mutex mtx;

#pragma omp parallel for
    for (uint16_t i = 0; i < visualBodyVertices.size(); i++) {
        auto proxyIndex = findClosestProxyIndex(visualBodyVertices[i].position);
        m_visualBodyToProxyIndices[i] = proxyIndex;
        mtx.lock();
        m_proxyToVisualBodyIndices[proxyIndex].push_back(i);
        mtx.unlock();
    }
}

void DynamicSoftBodyMapping::syncTriangles(const std::vector<uint16_t> &visualBodyIndices)
{
    m_proxyToVisualBodyTriangles.clear();
    m_proxyToVisualBodyTriangles.resize(m_proxyTriangles.getValue().size());

#pragma omp parallel for
    for (uint16_t i = 0; i < visualBodyIndices.size(); i+=3) {
        auto &p1 = m_visualBodyToProxyIndices[visualBodyIndices[i]];
        auto &p2 = m_visualBodyToProxyIndices[visualBodyIndices[i+1]];
        auto &p3 = m_visualBodyToProxyIndices[visualBodyIndices[i+2]];

        auto proxyTriIdx = m_topologyContainer->getTriangleIndex(p1, p2, p3);

        if (proxyTriIdx == sofa::core::topology::BaseMeshTopology::InvalidID){
            throw std::runtime_error("Cannot find a matching triagnle!");
        }
        else {
            m_proxyToVisualBodyTriangles[proxyTriIdx] = i/3;
        }
    }
}

void DynamicSoftBodyMapping::createTopologyChangeCallbacks()
{
    m_proxyTriangles.addTopologyEventCallBack(TopologyChangeType::TRIANGLESREMOVED, [this](const TopologyChange *topologyChange){
       std::cout << "FILASOFIA TRIANGLE REMOVED " << std::endl;

       // lock the body to perform changes
       auto vBody = m_visualBody.lock();
       if (!vBody) return;

       const auto &trisRemoved = reinterpret_cast<const TrianglesRemoved *>(topologyChange)->getArray();

       std::vector<uint16_t> visualTrisToRemove;

       for (const auto & triIdx: trisRemoved) {
           if (triIdx > m_proxyToVisualBodyTriangles.size())
               throw std::runtime_error("Trying to delete a triangle that is not mapped!");
           const auto &visualTriIdx = m_proxyToVisualBodyTriangles[triIdx];
           visualTrisToRemove.push_back(visualTriIdx);
       }

       std::sort(visualTrisToRemove.begin(), visualTrisToRemove.end(), std::greater<uint16_t>());

       auto &mesh = vBody->acquireMesh();
       uint16_t lastIndex = (mesh.indices().size())/3 - 1;
       mesh.removeTriangles(visualTrisToRemove);
       vBody->submitMesh();

       for (const auto &proxyTriIdx: trisRemoved) {
           m_proxyToVisualBodyTriangles[proxyTriIdx] = m_proxyToVisualBodyTriangles.back();
           m_proxyToVisualBodyTriangles.pop_back();
       }

       for (const auto &visualTriIdx: visualTrisToRemove) {
           std::replace(m_proxyToVisualBodyTriangles.begin(),
                        m_proxyToVisualBodyTriangles.end(),
                        lastIndex,
                        visualTriIdx);
           lastIndex--;
       }
    });

    m_proxyTriangles.addTopologyEventCallBack(TopologyChangeType::TRIANGLESADDED,
                                              [this](const TopologyChange* topologyChange) {
        std::cout << "FILASOFIA TRIANGLES ADDED" << std::endl;

        // mesh acquisition routine
        auto vBody = m_visualBody.lock();
        if (!vBody) return;
        auto &mesh = vBody->acquireMesh();

        // get the array of triangles added
        const auto &proxyTrisAdded = reinterpret_cast<const TrianglesAdded *>(topologyChange)->getElementArray();

        // vector of mapped visual indices per added triangle
        std::vector<uint16_t> visualIndices;
        visualIndices.reserve(proxyTrisAdded.size() * 3);

        // populate vector of mapped visual indices by adding
        // visual vertices that are mapped to the vertices of
        // created proxy triangles
        uint16_t nextVisualTriIndex = mesh.indices().size()/3;
        for (const auto &proxyTri: proxyTrisAdded) {
            for (const auto &proxyVertexIdx: proxyTri) {
                auto &map = m_proxyToVisualBodyIndices[proxyVertexIdx];
                // create a new vertex if no mapped visual vertex exists
                if (map.empty()) {
                    uint16_t nextVisualVertexIdx = mesh.vertices().size();
                    map.push_back(nextVisualVertexIdx);
                    m_visualBodyToProxyIndices.push_back(proxyVertexIdx);
                    auto mechState = m_topologyContainer->getContext()->getMechanicalState();
                    mesh.addVertices({{ mechState->getPX(proxyVertexIdx),
                                       mechState->getPY(proxyVertexIdx),
                                       mechState->getPZ(proxyVertexIdx)
                                     }});
                }
                visualIndices.push_back(map.front());
            }
            m_proxyToVisualBodyTriangles.push_back(nextVisualTriIndex);
            nextVisualTriIndex++;
        }

        mesh.addTriangles(visualIndices);

        vBody->submitMesh();
    });

    m_proxyPositions.addTopologyEventCallBack(TopologyChangeType::POINTSADDED,
                                              [this](const TopologyChange *topologyChange) {
        std::cout << "FILASOFIA POINTS ADDED" << std::endl;

        auto vBody = m_visualBody.lock();
        if (!vBody) return;

        auto &mesh = vBody->acquireMesh();
        uint16_t numOfVisualVertices = mesh.vertices().size();

        const auto &proxyPointsAdded = reinterpret_cast<const PointsAdded *>(topologyChange)->getElementArray();

        std::vector<filament::math::float3> positions;
        auto mechState = m_topologyContainer->getContext()->getMechanicalState();

        for (size_t i = 0; i < proxyPointsAdded.size(); ++i) {
            positions.push_back({ mechState->getPX(proxyPointsAdded[i]),
                            mechState->getPY(proxyPointsAdded[i]),
                            mechState->getPZ(proxyPointsAdded[i])
                          });
            // Add to proxyToVisual
            uint16_t visualIndex = numOfVisualVertices + i;
            m_proxyToVisualBodyIndices.push_back({visualIndex});

            // Add to visualToProxy
            m_visualBodyToProxyIndices.push_back(m_proxyToVisualBodyIndices.size()-1);
        }

        mesh.addVertices(positions);

        vBody->submitMesh();
    });

    m_proxyPositions.addTopologyEventCallBack(TopologyChangeType::POINTSREMOVED, [this](const TopologyChange *topologyChange) {
        std::cout << "FILASOFIA POINTS REMOVED" << std::endl;

        // lock the body to perform changes
        auto vBody = m_visualBody.lock();
        if (!vBody) return;

        const auto &proxyPointsRemoved = reinterpret_cast<const PointsRemoved *>(topologyChange)->getArray();
        std::vector<uint16_t> visualVerticesToRemove;

        // mark for removal each visual vertex that is mapped to any of the removed proxy vertices
        for (const auto &proxyIdx : proxyPointsRemoved) {
            // check if the proxy vertex is in the mapping list
            if (proxyIdx >= m_proxyToVisualBodyIndices.size())
                throw std::runtime_error("Proxy point does not exist in the mapping");

            // add each vertex from the vector of mapped visual points
            for (const auto &idx : m_proxyToVisualBodyIndices[proxyIdx]){
                visualVerticesToRemove.push_back(idx);
            }
        }

        // sort in descending order to perform removal with filling the gaps
        std::sort(visualVerticesToRemove.begin(), visualVerticesToRemove.end(), std::greater<uint16_t>());

        // lock mesh and remove vertices
        auto &mesh = vBody->acquireMesh();
        mesh.removeVertices(visualVerticesToRemove);
        vBody->submitMesh();

        // Update visualToProxy
        for (auto &idx: visualVerticesToRemove) {
            // replacing the removed visual vertex with the last visual vertex
            // also means that the mapping of the removed visual vertex should be
            // replaced with the mapping of the last visual vertex
            uint16_t lastIndex = m_visualBodyToProxyIndices.size() - 1;
            m_visualBodyToProxyIndices[idx] = m_visualBodyToProxyIndices[lastIndex] ;
            // also change corresponding entry in the proxy-to-visual mapping vector
            for (auto &vec: m_proxyToVisualBodyIndices) {
                std::replace(vec.begin(), vec.end(), lastIndex, idx);
            }
            m_visualBodyToProxyIndices.pop_back();
        }

        // Update proxyToVisual
        for (auto &idx: proxyPointsRemoved) {
            // same thing but now for the proxy mapping vector
            uint16_t lastIndex = m_proxyToVisualBodyIndices.size() - 1;
            m_proxyToVisualBodyIndices[idx] = m_proxyToVisualBodyIndices[lastIndex];
            // update corresponding entry in the visual-to-proxy mapping vector
            std::replace(m_visualBodyToProxyIndices.begin(), m_visualBodyToProxyIndices.end(), lastIndex, static_cast<uint16_t>(idx));
            m_proxyToVisualBodyIndices.pop_back();
        }        
    });
}

uint16_t DynamicSoftBodyMapping::findClosestProxyIndex(const filament::math::float3 &visualBodyVertex)
{
    uint16_t closestVertexIdx = 0;
    float minDistance= std::numeric_limits<float>::max();

    uint16_t numProxyPoints = m_proxyPositions.getValue().size();
    auto mechState = m_topologyContainer->getContext()->getMechanicalState();

    for (uint16_t i = 0; i < numProxyPoints; i ++) {
        // using manhattan distance for better performance
        float distance = std::abs(visualBodyVertex[0] - mechState->getPX(i))
                + std::abs(visualBodyVertex[1] - mechState->getPY(i))
                + std::abs(visualBodyVertex[2] - mechState->getPZ(i));

        if (distance < minDistance) {
            closestVertexIdx = i;
            minDistance = distance;
        }
    }

    return closestVertexIdx;
}

}
