#pragma once

#include "simulation/mapping/abstractmapping.h"
#include <sofa/component/topology/container/dynamic/TriangleSetTopologyModifier.h>
#include <sofa/component/topology/container/dynamic/TriangleSetTopologyContainer.h>
#include <sofa/component/topology/container/dynamic/TriangleSetGeometryAlgorithms.h>
#include <sofa/core/topology/TopologyData.h>

namespace filasofia {

class DynamicSoftBodyMapping : public AbstractMapping, public sofa::core::objectmodel::Base
{
public:
    DynamicSoftBodyMapping(const std::shared_ptr<PhysicalBody> &physicsBody,
                           const std::shared_ptr<VisualBody> &visualBody,
                           const std::shared_ptr<std::shared_mutex> &mutex);
    virtual ~DynamicSoftBodyMapping() override = default;

    virtual void update() override;
    virtual void init() override;

private:
    const std::weak_ptr<VisualBody> m_visualBody;

    std::vector<uint16_t> m_visualBodyToProxyIndices;
    std::vector<std::vector<uint16_t>> m_proxyToVisualBodyIndices;
    std::vector<uint16_t> m_proxyToVisualBodyTriangles;

    sofa::component::topology::container::dynamic::TriangleSetTopologyContainer::SPtr m_topologyContainer;
    sofa::component::topology::container::dynamic::TriangleSetTopologyModifier::SPtr m_topologyModifier;
    sofa::component::topology::container::dynamic::TriangleSetGeometryAlgorithms<sofa::defaulttype::Vec3Types>::SPtr m_geometryAlgorithms;

    typedef sofa::type::fixed_array<unsigned int, 3> ProxyTriangle;
    typedef sofa::type::vector<ProxyTriangle> ProxyTriangleVector;
    sofa::core::topology::TriangleData<ProxyTriangleVector> m_proxyTriangles;

    typedef sofa::defaulttype::Vec3dTypes::Coord ProxyPosition;
    typedef sofa::type::vector<ProxyPosition> ProxyPositionVector;
    sofa::core::topology::PointData<ProxyPositionVector> m_proxyPositions;

    void initProxyModel();
    void updateProxyPositions();

    void syncTopology(const filasofia::VisualMesh &visualMesh);
    void syncVertices(const std::vector<VertexData> &visualBodyVertices);
    void syncTriangles(const std::vector<uint16_t> &visualBodyIndices);

    void createTopologyChangeCallbacks();

    uint16_t findClosestProxyIndex(const filament::math::float3 &visualBodyVertex);

    bool m_topologyUpdateRequired {false};
};

}

