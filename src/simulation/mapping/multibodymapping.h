#pragma once

#include "rigidbodymapping.h"

namespace filasofia {

/**
 * @brief The MultiBodyMapping class
 *
 * Maps a vector rigid DOFs, e.g. in an articulated body,
 * to multiple visual bodies
 */
class MultiBodyMapping : public AbstractMapping
{
public:
    /**
     * @brief MultiBodyMapping ctor
     * @param physicsBody A pointer to the physics body
     * @param visualEntites A list of pointers to the visual entites
     * @param mutex The physics server synchronisation mutex
     * @param startIndex starting index of the physics body DoFs to map
     */
    MultiBodyMapping(const std::shared_ptr<PhysicalBody> &physicsBody,
                     const std::vector<std::shared_ptr<Transform>> &visualEntites,
                     const std::shared_ptr<std::shared_mutex> &mutex,
                     size_t startIndex = 0);

    virtual ~MultiBodyMapping() override = default;

    /**
     * @brief Update function
     *
     * Changes the transform of each visual body
     * according to the change of the corresponding rigid DoF
     */
    virtual void update() override;

    virtual void init() override {}

private:
    std::vector< std::weak_ptr<Transform> > m_visualEntities;
    size_t m_startIndex;
};

}

