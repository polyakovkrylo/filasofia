/**
 * @file The RigidBodyMapping class definition
 * @author Vladimir Poliakov
 */
#include "rigidbodymapping.h"

#include <sofa/component/statecontainer/MechanicalObject.h>

namespace filasofia {

using sofa::core::behavior::MechanicalState;
using sofa::defaulttype::Rigid3Types;
using filament::math::mat4f;

RigidBodyMapping::RigidBodyMapping(const std::shared_ptr<PhysicalBody> &physicsBody,
                                   const std::shared_ptr<Transform> &visualEntity,
                                   const std::shared_ptr<std::shared_mutex> &mutex) :
    AbstractMapping(physicsBody, mutex),
    m_visualEntity(visualEntity)
{

}

void RigidBodyMapping::update()
{
    auto visualEntity= m_visualEntity.lock();
    auto physicsBody = m_physicsBody.lock();

    if(!visualEntity || !physicsBody) {
        return;
    }

    m_mutex->lock_shared();

    auto pos = reinterpret_cast<MechanicalState<Rigid3Types> *>
            (physicsBody->node()->getMechanicalState())->readPositions();

    std::array<float, 16> transform;
    pos[0].writeOpenGlMatrix(transform.data());

    m_mutex->unlock_shared();

    visualEntity->setTransformMatrix(transform);
}

}
