#include "simulation/mapping/mappingfactory.h"

#include "rigidbodymapping.h"
#include "softbodymapping.h"
#include "multibodymapping.h"
#include "dynamicsoftbodymapping.h"

namespace filasofia {

using std::shared_ptr;
using std::unique_ptr;
using std::weak_ptr;
using std::shared_mutex;
using std::make_unique;
using std::initializer_list;
using std::vector;

using BodyType = PhysicalBody::BodyType;

std::unique_ptr<AbstractMapping> MappingFactory::create(const shared_ptr<PhysicalBody> &physicsBody,
                                                        const std::shared_ptr<Transform> &visualEntity,
                                                        const shared_ptr<shared_mutex> &mutex)
{
    unique_ptr<AbstractMapping> mapping;

    switch (physicsBody->type()) {
    case BodyType::SoftMassSpring:
    case BodyType::SoftTetrahedronFem:
        if (physicsBody->hasDynamicTopology())
            mapping = make_unique<DynamicSoftBodyMapping>(physicsBody,
                                                   std::dynamic_pointer_cast<VisualBody>(visualEntity),
                                                   mutex);
        else
            mapping = make_unique<SoftBodyMapping>(physicsBody,
                                                   std::dynamic_pointer_cast<VisualBody>(visualEntity),
                                                   mutex);
        break;
    case BodyType::Rigid:
        mapping = make_unique<RigidBodyMapping>(physicsBody, visualEntity, mutex);
        break;
    }

    return mapping;
}


std::unique_ptr<AbstractMapping> MappingFactory::create(const shared_ptr<PhysicalBody> &physicsBody,
                                                        const vector<shared_ptr<Transform>> &visualEntities,
                                                        const shared_ptr<shared_mutex> &mutex,
                                                        size_t startIndex)
{
    if (physicsBody->type() != BodyType::Rigid) {
        throw std::runtime_error("Cannot map multiple visual entities to non-rigid physical bodies!");
    }

    return make_unique<MultiBodyMapping>(physicsBody, visualEntities, mutex, startIndex);
}

}
