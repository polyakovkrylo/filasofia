#include "multibodymapping.h"

#include <sofa/core/behavior/MechanicalState.h>

namespace filasofia {

using std::vector;
using std::shared_ptr;
using std::weak_ptr;
using std::shared_mutex;

using sofa::core::behavior::MechanicalState;
using sofa::defaulttype::Rigid3Types;
using filament::math::mat4f;

MultiBodyMapping::MultiBodyMapping(const shared_ptr<PhysicalBody> &physicsBody,
                                   const vector<shared_ptr<Transform>> &visualEntities,
                                   const shared_ptr<shared_mutex> &mutex,
                                   size_t startIndex) :
    AbstractMapping(physicsBody, mutex),
    m_startIndex(startIndex)
{
    for (auto &e :visualEntities) {
        m_visualEntities.push_back(e);
    }
}

void MultiBodyMapping::update()
{
    auto physicsBody = m_physicsBody.lock();

    if(!physicsBody) {
        return;
    }


    auto pos = reinterpret_cast<MechanicalState<Rigid3Types> *>
            (physicsBody->node()->getMechanicalState())->readPositions();

    m_mutex->lock_shared();

    for (size_t i = 0; i < m_visualEntities.size(); i++) {
        auto visualEntity= m_visualEntities[i].lock();

        if(!visualEntity) {
            continue;;
        }

        std::array<float, 16> transform;
        pos[m_startIndex + i].writeOpenGlMatrix(transform.data());

        visualEntity->setTransformMatrix(transform);
    }

    m_mutex->unlock_shared();
}

}
