#include "simulation/simulation.h"

#include "simulation/mapping/mappingfactory.h"

namespace filasofia {

using std::make_unique;
using std::make_shared;
using std::string;
using std::chrono::milliseconds;
using std::shared_ptr;
using std::this_thread::sleep_until;
using std::vector;

using filasofia::window::SDLWindow;
using filasofia::Event;
using filasofia::PhysicalBody;

Simulation::Simulation(const string &name, const std::chrono::milliseconds &visualUpdatePeriod) :
    m_period{visualUpdatePeriod}
{
    m_window.reset(new SDLWindow(name, {{0,0},{1920,1080}}));
    m_window->init();

    m_eventDispatcher = make_shared<EventDispatcher>();

    m_visual = make_unique<VisualServer>(m_window->nativeWindow(),
                                         m_window->geometry().size.x(),
                                         m_window->geometry().size.y());
    m_physics = make_unique<filasofia::PhysicsServer>(m_eventDispatcher);

    m_physics->addDataRepository(DataRepository);
    m_physics->setSimulationType(filasofia::PhysicsServer::SimulationType::FreeMotionAnimation);
}

void Simulation::exec()
{
    m_physics->run();
    m_eventDispatcher->run();

    while(!m_physics->isInitialized()) {std::this_thread::yield();}
    initMappings();
    m_physics->initBodies();

    while(!m_close) {
        auto wakeup = std::chrono::steady_clock::now();
        auto ev = make_unique<Event>(Event::Type::VisualUpdate);
        m_eventDispatcher->post(ev);

        processWindowEvents();
        updateMappings();
        m_visual->render();


        sleep_until(wakeup+m_period);
    }

    m_physics->finish();
}

void Simulation::map(const shared_ptr<PhysicalBody> &physicsBody, const shared_ptr<Transform> &visualEntity)
{
    m_mappings.push_back(MappingFactory::create(physicsBody, visualEntity, m_physics->mutex()));
}

void Simulation::map(const shared_ptr<PhysicalBody> &physicsBody, const vector<shared_ptr<Transform>> &visualBodies, int startIndex)
{
    m_mappings.push_back(MappingFactory::create(physicsBody, visualBodies, m_physics->mutex(), startIndex));
}

void Simulation::processWindowEvents()
{
    auto ev = m_window->poll();
    switch (ev->type()) {
    case Event::Type::Quit:
        m_close = true;
        break;
    default:
        m_eventDispatcher->post(ev);
        break;
    }
}

void Simulation::updateMappings()
{
    m_physics->mutex()->lock_shared();
#pragma omp parallel for
    for (auto &mapping : m_mappings) {
        mapping->update();
    }
    m_physics->mutex()->unlock_shared();            
}

void Simulation::initMappings()
{
    m_physics->mutex()->lock_shared();
#pragma omp parallel for
    for (auto &mapping : m_mappings) {
        mapping->init();
    }
    m_physics->mutex()->unlock_shared();
}

}
