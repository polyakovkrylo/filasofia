#include "simulation/event/eventdispatcher.h"

#include "simulation/event/eventobserver.h"

namespace filasofia {

using std::shared_ptr;

EventDispatcher::EventDispatcher(const std::chrono::microseconds &period) :
    PeriodicTask(period, true)
{

}

void filasofia::EventDispatcher::attach(const shared_ptr<EventObserver> &observer)
{
    for (auto &filter : observer->filters()) {
        m_observers[filter].push_back(observer);
    }
}

void EventDispatcher::detach(const shared_ptr<EventObserver> &observer)
{
    // Seeking and erasing each pointer to the observer in the map
    for (auto &entry : m_observers) {
        auto &vec = entry.second;
        auto it = vec.begin();
        while (it != vec.end()) {
            if (it->lock() == observer)
                vec.erase(it);
            else
                ++it;
        }
    }
}

void filasofia::EventDispatcher::processEvent(const filasofia::Event &event)
{
    try {
        auto &vec =  m_observers.at(event.type());
        auto it = vec.begin();

        while (it != vec.end()) {
            auto observer = it->lock();

            // erase if the observer does not exist
            if (!observer)
                vec.erase(it);
            else {
                observer->processEvent(event);
                ++it;
            }
        }
    } catch (std::out_of_range e) {

    }
}

void EventDispatcher::processEvents()
{
    m_mutex.lock();
    while (!m_eventQueue.empty()) {
        auto &event = m_eventQueue.front();
        processEvent(*event);
        m_eventQueue.pop();
    }
    m_mutex.unlock();
}

void EventDispatcher::post(std::unique_ptr<Event> &event)
{
    m_mutex.lock();
    m_eventQueue.push(std::move(event));
    m_mutex.unlock();
}

void EventDispatcher::step(std::chrono::microseconds)
{
    processEvents();
}

}
