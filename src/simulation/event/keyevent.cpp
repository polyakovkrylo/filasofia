#include "simulation/event/keyevent.h"

namespace filasofia {

KeyEvent::KeyEvent(const Event::Type &type,
                   const KeyCode &key,
                   const bool &pressed,
                   const ModifierKeyFlags &modifiers) :
    Event(type),
    m_key(key),
    m_pressed(pressed),
    m_modifiers(modifiers)
{

}

}
