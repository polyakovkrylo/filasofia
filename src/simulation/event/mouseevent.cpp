#include "simulation/event/mouseevent.h"

namespace filasofia {

MouseEvent::MouseEvent(const Type &type,
                       const MouseEvent::ButtonFlags &buttons,
                       const MouseEvent::Point &position,
                       const ModifierKeyFlags &modifiers) :
    Event(type),
    m_buttons(buttons),
    m_position(position),
    m_modifiers(modifiers)
{

}

}
