#pragma once

#include <string>
#include <Eigen/Dense>

namespace filasofia {

bool endsWith(const std::string &str, const std::string &ending);

template<class Derived>
std::string toString(const Derived &m)
{
    std::stringstream stream;
    stream << m;
    return stream.str();
}

}

