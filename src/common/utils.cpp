#include "utils.h"

using std::string;

namespace filasofia {

bool endsWith(const string &str, const string &ending)
{
    return !str.compare(str.length() - ending.length(), ending.length(), ending);
}

}
