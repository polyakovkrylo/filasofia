/**
 * \file PeriodicTask class definition
 * \author Vladimir Poliakov
 */

#include "common/periodictask.h"

namespace filasofia {

using std::unique_ptr;
using std::chrono::microseconds;
using std::chrono::steady_clock;
using std::chrono::duration_cast;
using std::this_thread::sleep_until;
using std::this_thread::sleep_for;
using std::make_unique;
using std::thread;

PeriodicTask::PeriodicTask(microseconds period, bool realTime) :
    m_period(period),
    m_realTime(realTime)
{
}

void PeriodicTask::run()
{

    m_thread = make_unique<thread>([&] {
        init();

        m_finished = false;

        // current and previous timestamps
        steady_clock::time_point prev, current = steady_clock::now();

        // time ellapsed since last iteration
        microseconds dt;

        while (!m_finished) {
            // update time variables
            prev = current;
            current = steady_clock::now();
            dt = duration_cast<microseconds>(current - prev);

            step(dt);

            // Suspend till the next iteration
            if (m_realTime) {
                sleep_until(current + m_period);
            }
            else {
                sleep_for(m_period);
            }
        }

        cleanup();
    });
}

void PeriodicTask::finish()
{
    m_finished = true;
    m_thread->join();
    m_thread.release();
}

void PeriodicTask::init()
{
    m_initialized = true;
}

}
