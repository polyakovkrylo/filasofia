#include "common/filerepository.h"
#include <boost/filesystem.hpp>

using boost::filesystem::exists;
using boost::filesystem::is_directory;
using boost::filesystem::is_regular_file;
using boost::filesystem::directory_iterator;

using std::string;
using std::initializer_list;

namespace filasofia {

FileRepository DataRepository;

void FileRepository::addPath(const initializer_list<string> &paths, bool recursive)
{
    for (auto &path : paths) {
        addPath(path, recursive);
    }
}

void FileRepository::addPath(string path, bool recursive)
{
    if (exists(path) && is_directory(path)) {
        m_paths.push_back(path);

        if (recursive) {
            directory_iterator end;
            for(directory_iterator it(path); it != end; it++) {
                addPath(it->path().native(), recursive);
            }
        }
    }
}

const string FileRepository::find(const string &fileName)
{
    string result = string();

    for (auto path : m_paths) {
        path /= fileName;
        if (exists(path) && is_regular_file(path)) {
            result = path.native();
            break;
        }
    }

    return result;
}

}
