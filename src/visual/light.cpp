/**
 * @file Light class definition
 * @author Vladimir Poliakov
 */
#include "visual/light.h"

#include <filament/LightManager.h>
#include <utils/EntityManager.h>

namespace filasofia {

using filament::math::float3;
using filament::LightManager;
using utils::EntityManager;

Light::Light(std::shared_ptr<filament::Engine> &engine, LightManager::Builder &builder) :
    Transform(EntityManager::get().create(), engine)
{
    builder.build(*m_engine.get(), m_entity);
}

void Light::update()
{
    auto &lm = m_engine->getLightManager();
    auto transform = transformMatrix();

    if (m_entity){
        auto instance = lm.getInstance(*this);
        lm.setPosition(instance, transform[3].xyz);
        // A light's default direction is the same as for a camera
        auto dir = transform.toQuaternion() * float3(0,0,-1);
        lm.setDirection(instance, dir);
    }

    Transform::update();
}

}
