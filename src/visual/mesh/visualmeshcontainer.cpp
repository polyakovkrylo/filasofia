#include "visual/mesh/visualmeshcontainer.h"

#include <math/mat3.h>
#include <math/norm.h>

#include <string.h>

namespace filasofia {

using filament::math::float3;
using filament::math::mat3f;
using filament::math::packSnorm16;

using std::vector;

VisualMeshContainer::VisualMeshContainer(int refinementLevel) :
    m_refinementLevel{refinementLevel}
{

}

VisualMeshContainer::VisualMeshContainer(const std::vector<VertexData> &vertices, const std::vector<uint16_t> &indices, int refinementLevel) :
    m_vertices{vertices},
    m_indices{indices},
    m_refinementLevel{refinementLevel}
{

}

void VisualMeshContainer::removeVertices(const std::vector<uint16_t> &indicesToRemove)
{
    for (auto &idx: indicesToRemove) {
        uint16_t lastVertexIdx = m_vertices.size()-1;
        m_vertices[idx] = m_vertices[lastVertexIdx];
        std::replace(m_indices.begin(), m_indices.end(), lastVertexIdx, idx);

        for (auto &seamVec: m_seams) {
            seamVec.erase(std::remove(seamVec.begin(), seamVec.end(), idx), seamVec.end());
            std::replace(seamVec.begin(), seamVec.end(), lastVertexIdx, idx);
        }

        m_vertices.pop_back();
    }
}

void VisualMeshContainer::addVertices(const std::vector<filament::math::float3> &positions)
{
    for (const auto &pos: positions) {
        m_vertices.push_back(VertexData{ .position = pos });
    }
}

void VisualMeshContainer::removeTriangles(const std::vector<uint16_t> &triangleIndices)
{
    for (auto &triIdx : triangleIndices) {
        uint16_t idx =  triIdx*3;
        const auto &lastTriangleIt = m_indices.end() - 3;
        std::copy(lastTriangleIt, m_indices.end(), m_indices.begin() + idx);
        m_indices.resize(m_indices.size()-3);
    }
}

void VisualMeshContainer::addTriangles(const std::vector<uint16_t> &indices)
{
    m_indices.insert(m_indices.end(), indices.begin(), indices.end());
}

void VisualMeshContainer::recalculateTangents()
{
    // can be done offline
    auto vertexCount = m_vertices.size();
    auto indexCount = m_indices.size();

    vector<float3> tan1(vertexCount);
    vector<float3> tan2(vertexCount);

    memset(tan1.data(), 0, sizeof(float3) * vertexCount);
    memset(tan2.data(), 0, sizeof(float3) * vertexCount);

#pragma omp parallel for
    for (size_t i = 0; i < indexCount; i+=3) {
        const auto &v1 = m_vertices[m_indices[i]];
        const auto &v2 = m_vertices[m_indices[i+1]];
        const auto &v3 = m_vertices[m_indices[i+2]];

        float x1 = v2.position.x - v1.position.x;
        float x2 = v3.position.x - v1.position.x;
        float y1 = v2.position.y - v1.position.y;
        float y2 = v3.position.y - v1.position.y;
        float z1 = v2.position.z - v1.position.z;
        float z2 = v3.position.z - v1.position.z;

        float s1 = v2.uv.x - v1.uv.x;
        float s2 = v3.uv.x - v1.uv.x;
        float t1 = v2.uv.y - v1.uv.y;
        float t2 = v3.uv.y - v1.uv.y;
        float d = s1 * t2 - s2 * t1;

        float3 sdir, tdir;
        sdir = {t2 * x1 - t1 * x2, t2 * y1 - t1 * y2, t2 * z1 - t1 * z2};
        tdir = {s1 * x2 - s2 * x1, s1 * y2 - s2 * y1, s1 * z2 - s2 * z1};
        float r = 1.0f / d;
        sdir *= r;
        tdir *= r;

        tan1[m_indices[i]] += sdir;
        tan1[m_indices[i+1]] += sdir;
        tan1[m_indices[i+2]] += sdir;
        tan2[m_indices[i]] += tdir;
        tan2[m_indices[i+1]] += tdir;
        tan2[m_indices[i+2]] += tdir;
    }

    for (size_t i = 0; i < vertexCount; i++) {
        const float3& n = m_vertices[i].normal;
        const float3& t1 = tan1[i];
        const float3& t2 = tan2[i];

        // Gram-Schmidt orthogonalize
        float3 t = normalize(t1 - n * dot(n, t1));

        // Calculate handedness
        float w = (dot(cross(n, t1), t2) < 0.0f) ? -1.0f : 1.0f;

        float3 b = w < 0 ? cross(t, n) : cross(n, t);
        auto quat = mat3f::packTangentFrame({t, b, n});
        m_vertices[i].tangent = packSnorm16(quat.xyzw);
    }

}

void VisualMeshContainer::recalculateNormals()
{
    // Not an ideal implementation, need to find a better way to reset normals
    // All seams are visible

#pragma omp parallel for
    for (auto &v : m_vertices) {
        v.normal = {0,0,0};
    }

    // summ up normals of all adjacent faces for each vertex
#pragma omp parallel for
    for (size_t i = 0; i < m_indices.size(); i+=3) {
        auto &v1 = m_vertices[m_indices[i]];
        auto &v2 = m_vertices[m_indices[i+1]];
        auto &v3 = m_vertices[m_indices[i+2]];
        auto n = cross(v2.position-v1.position, v3.position-v1.position);

        v1.normal += n;
        v2.normal += n;
        v3.normal += n;
    }

#pragma omp parallel for
    for (auto &s : m_seams) {
        float3 normal {0};
        for (auto &v: s) normal += m_vertices[v].normal;
        for (auto &v: s) m_vertices[v].normal = normal;
    }

// normalize each normal
#pragma omp parallel for
    for (auto &v : m_vertices) {
        v.normal = normalize(v.normal);
    }
}

void VisualMeshContainer::populateSeams()
{
    m_seams.clear();

    // If the vertex has already been added to any of the
    // seams vectors, ignore it
    auto isInSeams = [&](uint16_t idx) -> bool {
        for (auto &s : m_seams) {
            if (std::find(s.begin(), s.end(), idx) != s.end())
                return true;
        }
        return false;
    };

    auto fuzzyEqual = [&](float3 p1, float3 p2) -> bool {
        // check if the distance in each direction is smaller than epsilon
        for (size_t i = 0; i < 3; ++i) {
             if (abs(p1[i] - p2[i]) > 1e-4)
                return false;
        }
        return true;
    };

    for (auto it = m_vertices.begin(); it != m_vertices.end(); it++) {
        auto it2 = it;
        std::vector<uint16_t> seam;
        uint16_t idx = std::distance(m_vertices.begin(), it);
        if (isInSeams(idx)) continue;
        seam.push_back(idx);

        // find all matches for the current vertex
        while (it2 != m_vertices.end()) {
            // find the iterator of the next matching vertex
            auto match = std::find_if(it2+1, m_vertices.end(), [&](VertexData &v)
            {
                    return fuzzyEqual(it->position, v.position);
            });

            // if a match found, add the vertex index to the seam
            if (match != m_vertices.end()) {
                uint16_t idx = std::distance(m_vertices.begin(), match);
                seam.push_back(idx);
            }

            it2 = match;
        }

        // if at least one element with  match was found, add indices to the seams vector
        if (seam.size() > 1)
            m_seams.push_back(seam);
    }
}

}
