#include "visual/mesh/visualmeshrefiner.h"
#include "visual/mesh/visualmesh.h"

#include <opensubdiv/far/topologyDescriptor.h>

namespace filasofia {
using std::vector;

using OpenSubdiv::Far::TopologyDescriptor;
using OpenSubdiv::Far::TopologyRefiner;
using TopologyRefinerFactory = OpenSubdiv::Far::TopologyRefinerFactory<TopologyDescriptor>;

static constexpr int NumVerticesPerFace = 3;

struct RefinedVertex {
    RefinedVertex() { }

    RefinedVertex(RefinedVertex const & src) {
        m_position[0] = src.m_position[0];
        m_position[1] = src.m_position[1];
        m_position[2] = src.m_position[2];
    }

    inline void Clear( void * =0 ) {
        m_position[0]=m_position[1]=m_position[2]=0.0f;
    }

    inline void AddWithWeight(RefinedVertex const & src, float weight) {
        m_position[0]+=weight*src.m_position[0];
        m_position[1]+=weight*src.m_position[1];
        m_position[2]+=weight*src.m_position[2];
    }

    inline void SetPosition(float x, float y, float z) {
        m_position[0]=x;
        m_position[1]=y;
        m_position[2]=z;
    }

    inline const float * GetPosition() const {
        return m_position;
    }

private:
    float m_position[3];
};

struct UvVertex {

    inline void Clear( void * =0 ) {
        m_uv[0] = m_uv[1] = 0.0f;
    }

    inline void AddWithWeight(UvVertex const & src, float weight) {
        m_uv[0] += weight*src.m_uv[0];
        m_uv[1] += weight*src.m_uv[1];
    }

    inline void SetUv(float u, float v)
    {
        m_uv[0] = u;
        m_uv[1] = v;
    }

    inline float * GetUv() { return m_uv; }

    float m_uv[2];
};


VisualMeshRefiner::VisualMeshRefiner(VisualMesh *coarseMesh,
                                     int refineLevel) :
    m_coarseMesh{ coarseMesh }
{
    setRefinementLevel(refineLevel);
}

void VisualMeshRefiner::init()
{
    // casting short vector to int vector for compatability
    m_coarseMeshFVarIndices = m_coarseMeshGridIndices = std::vector<int>(m_coarseMesh->indices().begin(),
                                                                         m_coarseMesh->indices().end());

    for (auto &seamVec : m_coarseMesh->seams()) {
        auto comp = [&](int index) {
            return std::find(seamVec.begin(), seamVec.end(), index) != seamVec.end();
        };
        std::replace_if(m_coarseMeshGridIndices.begin(),
                        m_coarseMeshGridIndices.end(),
                        comp, seamVec[0]
        );
    }

    int numFaces = m_coarseMesh->indices().size() / NumVerticesPerFace;
    vector<int> vertPerFace(numFaces, NumVerticesPerFace);

    OpenSubdiv::Sdc::SchemeType type = OpenSubdiv::Sdc::SCHEME_LOOP;

    OpenSubdiv::Sdc::Options options;
    options.SetVtxBoundaryInterpolation(OpenSubdiv::Sdc::Options::VTX_BOUNDARY_EDGE_AND_CORNER);

    OpenSubdiv::Far::TopologyDescriptor descriptor;
    descriptor.numVertices = m_coarseMesh->vertices().size();
    descriptor.numFaces = numFaces;
    descriptor.numVertsPerFace = vertPerFace.data();
    descriptor.vertIndicesPerFace = m_coarseMeshGridIndices.data();

    OpenSubdiv::Far::TopologyDescriptor::FVarChannel uvChannel;
    uvChannel.numValues = m_coarseMesh->vertices().size();
    uvChannel.valueIndices = m_coarseMeshFVarIndices.data();

    descriptor.numFVarChannels = 1;
    descriptor.fvarChannels = &uvChannel;

    m_refiner.reset(
                TopologyRefinerFactory::Create(
                    descriptor,
                    TopologyRefinerFactory::Options(type, options))
                );

    TopologyRefiner::UniformOptions refineOptions(refinementLevel());
    refineOptions.fullTopologyInLastLevel =true;
    m_refiner->RefineUniform(refineOptions);

    m_vertices.resize(m_refiner->GetLevel(m_refinementLevel).GetNumFVarValues());
    m_indices.resize(m_refiner->GetLevel(m_refinementLevel).GetNumFaceVertices());

    update();

    populateSeams();

    recalculateNormals();
    recalculateTangents();
}

void VisualMeshRefiner::update()
{
    std::vector<RefinedVertex> vertices(m_refiner->GetNumVerticesTotal());
    RefinedVertex *vertexPtr = vertices.data();

    std::vector<UvVertex> uvVertices(m_refiner->GetNumFVarValuesTotal());
    UvVertex *uvVertexPtr = uvVertices.data();

// update coarse mesh vertex positions
#pragma omp parallel for
    for (unsigned long i = 0; i < m_coarseMesh->vertices().size(); i++) {
        const auto &v = m_coarseMesh->vertices()[i];
        vertices[i].SetPosition(v.position[0], v.position[1], v.position[2]);
        uvVertices[i].SetUv(v.uv[0], v.uv[1]);
    }

// refine for each level
    OpenSubdiv::Far::PrimvarRefiner primvarRefiner(*m_refiner);
    RefinedVertex *src = vertexPtr;
    UvVertex *srcUv = uvVertexPtr;
    for (int level = 1; level <= m_refinementLevel; ++level) {
        RefinedVertex *dst = src + m_refiner->GetLevel(level-1).GetNumVertices();
        UvVertex *dstUv = srcUv + m_refiner->GetLevel(level-1).GetNumFVarValues();
        primvarRefiner.Interpolate(level, src, dst);
        primvarRefiner.InterpolateFaceVarying(level, srcUv, dstUv);
        src = dst;
        srcUv = dstUv;
    }

// update output
    const auto &refLastLevel = m_refiner->GetLevel(m_refinementLevel);
    const auto &vertIdxOffset = m_refiner->GetNumVerticesTotal() - refLastLevel.GetNumVertices();
    const auto &uvIdxOffset = m_refiner->GetNumFVarValuesTotal() - refLastLevel.GetNumFVarValues();

#pragma omp parallel for
    for (int faceIdx = 0; faceIdx < refLastLevel.GetNumFaces(); faceIdx++) {
        auto vtxIndices = refLastLevel.GetFaceVertices(faceIdx);
        auto uvIndices = refLastLevel.GetFaceFVarValues(faceIdx);

        assert(vtxIndices.size() == uvIndices.size());
        assert(vtxIndices.size() == NumVerticesPerFace);

        for (int i = 0; i < vtxIndices.size(); i++) {
            const auto &pos = vertices[vertIdxOffset + vtxIndices[i]].GetPosition();
            const auto &uv = uvVertices[uvIdxOffset + uvIndices[i]].GetUv();

            // Using uv index as vertex vector does not include seams (no double vertices)
            auto &v = m_vertices[uvIndices[i]];
            v.position = {pos[0], pos[1], pos[2]};
            v.uv = {uv[0], uv[1]};
        }

        auto idxOffset = faceIdx * NumVerticesPerFace;
        m_indices[idxOffset] = uvIndices[0];
        m_indices[idxOffset + 1] = uvIndices[1];
        m_indices[idxOffset + 2] = uvIndices[2];
    }

    recalculateNormals();
    recalculateTangents();
}


}
