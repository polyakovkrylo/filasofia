#include "visual/mesh/visualmeshimporter.h"
#include "common/filerepository.h"

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/mesh.h>
#include <assimp/postprocess.h>

namespace filasofia {

using Assimp::Importer;
using std::vector;

void populateVertices(aiMesh &source, vector<VertexData> &destination);
void populatePositions(aiMesh &source, vector<VertexData> &destination);
void populateNormals(aiMesh &source, vector<VertexData> &destination);
void populateTextureCoordinates(aiMesh &source, vector<VertexData> &destination);
void populateIndices(aiMesh &source, vector<uint16_t> &destination);

VisualMesh importVisualMesh(const std::string &filename, int refinementLevel)
{
    Importer importer;
    auto path = filasofia::DataRepository.find(filename);
    const aiScene *scene = importer.ReadFile(path,
                                             aiProcess_JoinIdenticalVertices | aiProcess_Triangulate);

    // We only process the first mesh in the file
    auto importedMesh = scene->mMeshes[0];

    VisualMesh mesh;

    vector<VertexData> vertices;
    vector<uint16_t> indices;

    populateVertices(*importedMesh, vertices);
    populateIndices(*importedMesh, indices);

    mesh.setVertices(vertices);
    mesh.setIndices(indices);
    mesh.setRefinementLevel(refinementLevel);

    return mesh;
}

void populateVertices(aiMesh &source, vector<VertexData> &destination)
{
    destination.resize(source.mNumVertices);

    populatePositions(source, destination);

    // we only work with uv0
    if (source.HasTextureCoords(0))
        populateTextureCoordinates(source, destination);

    if (source.HasNormals())
        populateNormals(source, destination);
}

void populatePositions(aiMesh &source, vector<VertexData> &destination)
{
#pragma omp parallel for
    for (unsigned int i = 0; i < source.mNumVertices; i++) {
        auto &v = source.mVertices[i];
        destination.at(i).position = {v.x, v.y, v.z};
    }
}

void populateNormals(aiMesh &source, vector<VertexData> &destination)
{
#pragma omp parallel for
    for (unsigned int i = 0; i < source.mNumVertices; i++) {
        auto &n = source.mNormals[i];
        destination.at(i).normal = {n.x, n.y, n.z};
    }
}

void populateTextureCoordinates(aiMesh &source, vector<VertexData> &destination)
{
#pragma omp parallel for
        for (unsigned int i = 0; i < source.mNumVertices; i++) {
            auto &uv = source.mTextureCoords[0][i];
            destination.at(i).uv = {uv.x, uv.y};
        }
}

void populateIndices(aiMesh &source, vector<uint16_t> &destination)
{
    // We only accept triangle faces
    destination.resize(source.mNumFaces * 3);

#pragma omp parallel for
    for (unsigned int i = 0; i < source.mNumFaces; i++) {
        auto &f = source.mFaces[i];
        destination.at(i*3) = f.mIndices[0];
        destination.at((i*3) + 1) = f.mIndices[1];
        destination.at((i*3) + 2) = f.mIndices[2];
    }
}

}
