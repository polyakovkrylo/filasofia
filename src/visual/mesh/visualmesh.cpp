/**
 * @file VisualServer class definition
 * @author Vladimir Poliakov
 */
#include "visual/mesh/visualmesh.h"
#include "visual/mesh/visualmeshrefiner.h"

namespace filasofia {

using filament::IndexBuffer;
using filament::VertexBuffer;
using filament::VertexAttribute;

using std::make_unique;

VisualMesh::VisualMesh(int refinementLevel) :
    VisualMeshContainer(refinementLevel)
{
}

VisualMesh::VisualMesh(const std::vector<VertexData> &vertices, const std::vector<uint16_t> &indices, int refinementLevel) :
    VisualMeshContainer(vertices,indices, refinementLevel)
{
}

VisualMesh::VisualMesh(VisualMesh &rhs)
{
    m_engine = rhs.m_engine;
    m_vertices = rhs.m_vertices;
    m_indices = rhs.m_indices;
    m_seams = rhs.m_seams;
    m_vertexBuffer = FilamentScopedPointer<VertexBuffer>(rhs.m_vertexBuffer.release(), {m_engine});
    m_vertexBuffer = FilamentScopedPointer<VertexBuffer>(rhs.m_vertexBuffer.release(), {m_engine});
}

void VisualMesh::init()
{
    if (!m_engine)
        return;

    populateSeams();

    if (m_refinementLevel) {
        m_refiner = std::make_unique<VisualMeshRefiner>(this, m_refinementLevel);
        m_refiner->init();
        m_container = m_refiner.get();
    }

    initVertexBuffer(m_container->vertices().size() * 2);
    initIndexBuffer(m_container->indices().size() * 2);

    update();
}

void VisualMesh::update()
{
    if (m_topologyUpdateRequired) {
        if (m_refiner) m_refiner->init();
        if (m_topologyUpdateCallback) m_topologyUpdateCallback();
        m_topologyUpdateRequired = false;
    }

    if (m_refiner) {
        m_refiner->update();
    }
    else {
        recalculateNormals();
        recalculateTangents();
    }

    if (m_container->vertices().size() > m_vertexBuffer->getVertexCount())
        initVertexBuffer(m_container->vertices().size() * 2);

    if (m_container->indices().size() > m_indexBuffer->getIndexCount())
        initIndexBuffer(m_container->indices().size() * 2);


    m_vertexBuffer->setBufferAt(*m_engine, 0,
                                VertexBuffer::BufferDescriptor(
                                    m_container->vertices().data(),
                                    m_container->vertices().size() * sizeof(VertexData),
                                    nullptr)
                                );

    m_indexBuffer->setBuffer(*m_engine,
                             IndexBuffer::BufferDescriptor(
                                 m_container->indices().data(),
                                 m_container->indices().size() * sizeof (uint16_t),
                                 nullptr)
                             );
}

void VisualMesh::removeVertices(const std::vector<uint16_t> &vertexIndices)
{
    VisualMeshContainer::removeVertices(vertexIndices);
    m_topologyUpdateRequired = true;
}

void VisualMesh::addVertices(const std::vector<filament::math::float3> &positions)
{
    VisualMeshContainer::addVertices(positions);
    m_topologyUpdateRequired = true;
}

void VisualMesh::removeTriangles(const std::vector<uint16_t> &triangleIndices)
{
    VisualMeshContainer::removeTriangles(triangleIndices);
    m_topologyUpdateRequired = true;
}

void VisualMesh::addTriangles(const std::vector<uint16_t> &indices)
{
    VisualMeshContainer::addTriangles(indices);
    m_topologyUpdateRequired = true;
}

void VisualMesh::initIndexBuffer(size_t indexCount)
{
    IndexBuffer* ib = IndexBuffer::Builder()
            .indexCount(indexCount)
            .bufferType(IndexBuffer::IndexType::USHORT)
            .build(*m_engine);


    m_indexBuffer = FilamentScopedPointer<IndexBuffer>(std::move(ib), {m_engine});
}

void VisualMesh::initVertexBuffer(size_t vertexCount)
{
    VertexBuffer *vb = VertexBuffer::Builder()
            .vertexCount(vertexCount)
            .bufferCount(1)
            .attribute(VertexAttribute::POSITION, 0, VertexBuffer::AttributeType::FLOAT3,
                       0, sizeof(VertexData))
            .attribute(VertexAttribute::TANGENTS, 0, VertexBuffer::AttributeType::SHORT4,
                       sizeof(VertexData::position), sizeof(VertexData))
            .attribute(VertexAttribute::UV0, 0, VertexBuffer::AttributeType::FLOAT2,
                       sizeof(VertexData::position) + sizeof(VertexData::tangent), sizeof(VertexData))
            .normalized(VertexAttribute::TANGENTS)
            .build(*m_engine);

    m_vertexBuffer = FilamentScopedPointer<VertexBuffer>(std::move(vb), {m_engine});
}
}
