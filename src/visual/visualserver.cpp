/**
 * @file VisualServer class definition
 * @author Vladimir Poliakov
 */
#include "visual/visualserver.h"

#include "common/utils.h"
#include "common/filerepository.h"

#include <filament/Viewport.h>
#include <filament/Fence.h>
#include <utils/EntityManager.h>
#include <image/Ktx1Bundle.h>
#include <ktxreader/Ktx1Reader.h>

#include <fstream>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

namespace filasofia {

using filament::Engine;
using filament::SwapChain;
using filament::Renderer;
using filament::Scene;
using filament::View;
using filament::Viewport;
using filament::Material;
using filament::Texture;
using filament::Skybox;
using filament::IndirectLight;
using filament::MultiSampleAntiAliasingOptions;

using image::Ktx1Bundle;
using utils::EntityManager;

using std::shared_ptr;
using std::unique_ptr;
using std::make_unique;
using std::make_shared;
using std::ifstream;
using std::vector;

using filasofia::endsWith;

VisualServer::VisualServer(void *window, uint32_t windowWidth, uint32_t windowHeight, bool createDefaultViewAndCamera) :
    m_nativeWindowHandler (window),
    m_windowWidth(windowWidth),
    m_windowHeight(windowHeight)
{
    m_engine.reset(Engine::create(Engine::Backend::VULKAN),
             [](Engine *engine) {engine->destroy(&engine);});

    // Create swap chain
    m_swapChain.reset(m_engine->createSwapChain(m_nativeWindowHandler));

    if (!m_swapChain) {
        throw std::runtime_error("Cannot create swap chain");
    }

    // Create renderer
    m_renderer.reset(m_engine->createRenderer());

    // Create scene
    m_scene.reset(m_engine->createScene());

    if (createDefaultViewAndCamera)
        createViewAndCamera("default");
}

VisualServer::~VisualServer()
{
    filament::Fence::waitAndDestroy(m_engine->createFence());
}

void VisualServer::createViewAndCamera(const std::string &name)
{
    // Create default camera and view
    auto &cam = createCamera(name);
    auto &view = createView(name);
    view->setCamera(cam->instance());
}

void VisualServer::render()
{
    for (auto &body : m_bodies) body->update();

    if (!m_renderer->beginFrame(m_swapChain.get()))
        return;

    for(auto &view : m_views) {
        m_renderer->render(view.second.get());
    }

    m_renderer->endFrame();
}

void VisualServer::createSkybox(const std::string &fileName)
{
    addTexture("Skybox", fileName);
    auto &tex = texture("Skybox");
    m_skybox.reset(Skybox::Builder().environment(tex.get()).build(*m_engine));
    m_scene->setSkybox(m_skybox.get());
}

void VisualServer::createIbl(const std::string &filename, float intensity)
{
    addTexture("IBL", filename);
    auto tex = texture("IBL");
    m_ibl.reset(IndirectLight::Builder()
                .intensity(intensity)
                .reflections(tex.get())
                .build(*m_engine));
    m_scene->setIndirectLight(m_ibl.get());
}

const FilamentScopedPointer<View> &VisualServer::createView(const std::string &name, const filament::math::float4 &viewport)
{
    auto view = m_engine->createView();

    view->setAntiAliasing(View::AntiAliasing::FXAA);
//    view->setBlendMode(View::BlendMode::TRANSLUCENT);
    view->setPostProcessingEnabled(true);

    MultiSampleAntiAliasingOptions msaaOptions;
    msaaOptions.enabled = true;
    view->setMultiSampleAntiAliasingOptions(msaaOptions);

    // translate relative values (0.0, 1.0) to absolute values
    view->setViewport({static_cast<int32_t>(viewport[0] * m_windowWidth),
                       static_cast<int32_t>(viewport[1] * m_windowHeight),
                       static_cast<uint32_t>(viewport[2] * m_windowWidth),
                       static_cast<uint32_t>(viewport[3] * m_windowHeight)
                      });

    view->setScene(m_scene.get());

    m_views[name] = FilamentScopedPointer<View>(view);

    return m_views[name];
}

const shared_ptr<Camera> &VisualServer::createCamera(const std::string &name, double fov, double aspect, double near, double far)
{
    auto cam = make_shared<Camera>(m_engine);
    cam->instance()->setProjection(fov, aspect, near, far);

    m_cameras[name] = cam;

    return m_cameras[name];
}

void VisualServer::addMaterial(const std::string &name, const std::string &fileName)
{
    auto path = filasofia::DataRepository.find(fileName);

    ifstream file(path);

    if(!file)
        throw std::runtime_error("Material file does not exist");

    file.seekg(0, file.end);
    auto length = file.tellg();
    file.seekg(0, file.beg);

    char *buffer= new char[static_cast<unsigned long>(length)];

    file.read(buffer, length);

    Material *material = Material::Builder()
            .package(buffer, static_cast<unsigned long>(length))
            .build(*m_engine);

    m_materials[name] = shared_ptr<Material>(material, [&](Material *material){
            m_engine->destroy(material);
});

    delete[] buffer;
}

void VisualServer::addTexture(const std::string &name, const std::string &fileName)
{
    auto &path = filasofia::DataRepository.find(fileName);
    if (path.empty())
        return;

    Texture *texture;
    if (endsWith(fileName, ".ktx")) {
        ifstream file(path, std::ios::binary);
        vector<uint8_t> contents((std::istreambuf_iterator<char>(file)), {});
        // the bundle is deleted automatically in createTexture
        Ktx1Bundle *ktx  = new Ktx1Bundle(contents.data(), static_cast<uint32_t>(contents.size()));
        auto freeKtx = [] (void* userdata) {
                Ktx1Bundle* ktx = (Ktx1Bundle*) userdata;
                delete ktx;
            };
        texture = ktxreader::Ktx1Reader::createTexture(m_engine.get(), *ktx, false, freeKtx, ktx);
    } else {
        int texWidth{0}, texHeight{0}, texChannels{0};
        unsigned char *data = stbi_load(path.c_str(), &texWidth, &texHeight, &texChannels, STBI_rgb_alpha);
        texture = Texture::Builder()
                .width(uint32_t(texWidth))
                .height(uint32_t(texHeight))
                .levels(0xff)
                .format(Texture::InternalFormat::RGBA8)
                .build(*m_engine);

        Texture::PixelBufferDescriptor buffer(data, size_t(texWidth * texHeight * texChannels),
                                              Texture::Format::RGBA, Texture::Type::UBYTE,
                                              reinterpret_cast<Texture::PixelBufferDescriptor::Callback>(&stbi_image_free));
        texture->setImage(*m_engine, 0, std::move(buffer));
        texture->generateMipmaps(*m_engine);
    }

    m_textures[name] = shared_ptr<Texture>(texture, [&](Texture *t) {
            m_engine->destroy(t);
});
}

const std::shared_ptr<VisualBody> VisualServer::addVisualBody(VisualBody::Builder &builder)
{
    // Entity instanition is finished in build(), builder
    // is used to pass the parameters
    m_bodies.push_back(builder.build(m_engine));
    m_scene->addEntity(*m_bodies.back());
    return m_bodies.back();
}

const std::shared_ptr<Transform> VisualServer::addTransform(const Eigen::Translation3f &translation, const Eigen::Quaternionf &rotation)
{
    m_transforms.push_back(make_shared<Transform>(m_engine->getEntityManager().create(), m_engine));
    auto &entity = m_transforms.back();
    entity->setFromPose(translation, rotation);
    return entity;
}

const std::shared_ptr<Light> &VisualServer::addLight(filament::LightManager::Builder &builder)
{
    // Entity instanition is finished in the Light ctor, builder
    // is used to pass the parameters
    m_lights.push_back(make_unique<Light>(m_engine, builder));
    m_scene->addEntity(*m_lights.back());
    return m_lights.back();
}

}
