#include "visual/camera.h"

#include <utils/EntityManager.h>

namespace filasofia {

using utils::EntityManager;
using filament::Engine;

Camera::Camera(const std::shared_ptr<Engine> engine) :
    Transform(EntityManager::get().create(), engine)
{
    m_camera = m_engine->createCamera(m_entity);
}

}
