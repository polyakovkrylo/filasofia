/**
 * @file VisualBody class definition
 * @author Vladimir Poliakov
 */
#include "visual/visualbody.h"

#include <filament/RenderableManager.h>
#include <utils/EntityManager.h>

namespace filasofia {

using filament::MaterialInstance;
using filament::TextureSampler;
using utils::EntityManager;
using filament::RenderableManager;
using filament::math::mat4f;
using filament::math::float3;

VisualBody::Builder::Builder(const std::string &name)
{
    m_body.reset(new VisualBody(name));
}

VisualBody::Builder &VisualBody::Builder::mesh(const VisualMesh &mesh)
{
    m_body->m_mesh.setVertices(mesh.vertices());
    m_body->m_mesh.setIndices(mesh.indices());
    m_body->m_mesh.setSeams(mesh.seams());
    m_body->m_mesh.setRefinementLevel(mesh.refinementLevel());
    return *this;
}

VisualBody::Builder &VisualBody::Builder::material(const std::shared_ptr<filament::Material> &material)
{
    m_body->m_material = material;
    return *this;
}

VisualBody::Builder &VisualBody::Builder::addTexture(const std::string &name, const std::shared_ptr<filament::Texture> &texture)
{
    m_body->m_textures[name] = texture;
    return *this;
}

std::shared_ptr<VisualBody> VisualBody::Builder::build(std::shared_ptr<filament::Engine> engine)
{
    m_body->m_engine = engine;
    m_body->init();
    return std::move(m_body);
}

void VisualBody::init()
{
    m_mesh.setEngine(m_engine);
    m_mesh.init();

    m_materialInstance.get_deleter().m_engine = m_engine;
    m_materialInstance.reset(m_material->createInstance(m_name.c_str()));

    m_textureSampler.setMinFilter(TextureSampler::MinFilter::LINEAR_MIPMAP_LINEAR);
    m_textureSampler.setMagFilter(TextureSampler::MagFilter::LINEAR);

    for(auto &tex : m_textures) {
        m_materialInstance->setParameter(tex.first.c_str(), tex.second.get(), m_textureSampler);
    }
    m_materialInstance->setCullingMode(filament::Material::CullingMode::NONE);

    set_engine(m_engine);
    set_entity(FilamentScopedEntity(utils::EntityManager::get().create(), m_engine));

    RenderableManager::Builder(1)
            .boundingBox(calculateAabb())
            .geometry(0, RenderableManager::PrimitiveType::TRIANGLES, m_mesh.vertexBuffer().get(), m_mesh.indexBuffer().get())
            .material(0, m_materialInstance.get())
            .castShadows(true)
            .receiveShadows(true)
            .build(*m_engine, *this);

    m_mesh.setTopologyUpdateCallback([&]() {
        const auto &instance = m_engine->getRenderableManager().getInstance(*this);
        m_engine->getRenderableManager().setGeometryAt(instance, 0, RenderableManager::PrimitiveType::TRIANGLES,
                                                       m_mesh.vertexBuffer().get(), m_mesh.indexBuffer().get(),
                                                       0, m_mesh.indexCount());
    });
}



VisualMesh &VisualBody::acquireMesh()
{
    m_mutex.lock();
    return m_mesh;
}

void VisualBody::submitMesh()
{
    m_mutex.unlock();
}

void VisualBody::update()
{
    Transform::update();

    m_mutex.lock();
    m_mesh.update();
    updateAabb();
    m_mutex.unlock();
}

VisualBody::VisualBody(const std::string &name) :
    m_name(name)
{

}

filament::Box VisualBody::calculateAabb()
{
    float3 bmin(std::numeric_limits<float>::max());
    float3 bmax(std::numeric_limits<float>::lowest());
    filament::Box aabb;

    for (auto &vertex : m_mesh.vertices()) {
        bmin.x = std::min(bmin.x, vertex.position.x);
        bmin.y = std::min(bmin.y, vertex.position.y);
        bmin.z = std::min(bmin.z, vertex.position.z);

        bmax.x = std::max(bmax.x, vertex.position.x);
        bmax.y = std::max(bmax.y, vertex.position.y);
        bmax.z = std::max(bmax.z, vertex.position.z);
    }

    return aabb.set(bmin, bmax);
}

void VisualBody::updateAabb()
{
    auto &rm = m_engine->getRenderableManager();
    rm.setAxisAlignedBoundingBox(rm.getInstance(*this), calculateAabb());
}

}
