#include "visual/transform.h"

#include <filament/TransformManager.h>

namespace filasofia {

using filament::math::mat4f;
using filament::math::quatf;
using vec3f = filament::math::vec3<float>;

using Eigen::Translation3f;
using Eigen::Quaternionf;
using Eigen::Vector3f;

filasofia::Transform::Transform(filasofia::Transform *parent) :
    m_parent(parent)
{

}

filasofia::Transform::Transform(utils::Entity &&entity, std::shared_ptr<filament::Engine> engine, Transform *parent) :
    FilamentScopedEntity(std::move(entity), engine),
    m_parent(parent)
{

}

filasofia::Transform::Transform(const std::shared_ptr<filament::Engine> &engine, Transform *parent) :
    FilamentScopedEntity(engine),
    m_parent(parent)
{

}

filasofia::Transform::~Transform()
{
    // set the parent of each child to this entity's parent
    for(auto &child: m_children) {
        child->setParent(m_parent);
    }

    // remove the entity from the parent's children list
    if(m_parent)
        m_parent->removeChild(this);
}

filament::math::mat4f Transform::transformMatrix()
{
    if (m_parent)
        return m_parent->transformMatrix() * m_transformMatrix;
    else
        return m_transformMatrix;
}

void filasofia::Transform::setTransformMatrix(const filament::math::mat4f &transformMatrix)
{
    m_transformMatrix = transformMatrix;
    update();
}

void Transform::setTransformMatrix(const std::array<float,16> &transformMatrix)
{
    mat4f mat;

    for (size_t i = 0; i < 4; i++) {
        mat[i][0] = transformMatrix[4*i];
        mat[i][1] = transformMatrix[4*i + 1];
        mat[i][2] = transformMatrix[4*i + 2];
        mat[i][3] = transformMatrix[4*i + 3];
    }

    setTransformMatrix(mat);
}

void filasofia::Transform::setFromPose(const Eigen::Translation3f &translation, const Eigen::Quaternionf &rotation)
{
    setTransformMatrix(mat4f(quatf(rotation.w(), rotation.x(), rotation.y(), rotation.z())) *
                 mat4f::translation(vec3f(translation.x(),translation.y(),translation.z()))
                 );
}

void filasofia::Transform::translate(const Eigen::Vector3f translation)
{
    m_transformMatrix = m_transformMatrix.translation(toFilamentVec3(translation));
    update();
}

void filasofia::Transform::rotate(const Eigen::Quaternionf rotation)
{
    m_transformMatrix = mat4f(toFilamentQuat(rotation)) * m_transformMatrix;
    update();
}

void filasofia::Transform::addChild(filasofia::Transform *child)
{
    m_children.push_back(child);
    update();
}

void filasofia::Transform::removeChild(filasofia::Transform *child)
{
    m_children.erase(std::remove_if(m_children.begin(),
                                    m_children.end(),
                                    [&](Transform *member) { return member == child; }),
            m_children.end());
    update();
}

void filasofia::Transform::setParent(filasofia::Transform *parent)
{
    if (m_parent)
        m_parent->removeChild(this);

    m_parent = parent;
    m_parent->addChild(this);
    update();
}

void filasofia::Transform::update()
{
    auto &tm = m_engine->getTransformManager();
    auto transform = transformMatrix();

    if (m_entity)
        tm.setTransform(tm.getInstance(*this), transform);

    for (auto &child: m_children) {
        child->update();
    }
}

filament::math::vec3<float> Transform::toFilamentVec3(const Eigen::Vector3f &vec)
{
    return filament::math::vec3<float>(vec.x(), vec.y(), vec.z());
}

filament::math::quatf Transform::toFilamentQuat(const Eigen::Quaternionf &quat)
{
    return quatf(quat.w(), quat.x(), quat.y(), quat.z());
}

}
