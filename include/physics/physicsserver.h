/**
 * @file The PhysicsServer class declaration
 * @author Vladimir Poliakov
 */
#pragma once

#include "common/periodictask.h"

#include "physicalbody.h"
#include "hapticinterface.h"
#include "articulation.h"
#include "common/filerepository.h"

#include <sofa/simulation/graph/DAGNode.h>
#include <sofa/simulation/graph/DAGSimulation.h>
#include <sofa/helper/system/FileRepository.h>

#include <string>
#include <vector>
#include <unordered_set>
#include <shared_mutex>

#include <Eigen/Dense>

namespace filasofia {

class EventDispatcher;

/**
 * @brief The PhysicsServer class
 */
class PhysicsServer : public PeriodicTask
{
public:
    /**
     * @brief The SimulationType enum
     */
    enum class SimulationType {
        Default,
        FreeMotionAnimation
    };

    /**
     * @brief Class constructor
     * @param dispatcher Pointer to the event dispatcher to which the server sends events
     * @param period Period of iteration
     * @param slack Maximum allowed dt for a simulation step
     */
    PhysicsServer(const std::shared_ptr<EventDispatcher> &dispatcher,
                  const std::chrono::microseconds &period = std::chrono::microseconds(5000),
                  const std::chrono::microseconds &slack = std::chrono::microseconds(2000));

    /**
     * @brief Set period
     * @param period Period of iteration
     * @param slack Maximal allowed slack
     */
    void setSlack(std::chrono::microseconds slack);

    /**
     * @brief Set simulation type
     * @param type Type of simulation
     * @note Can only be called before init()
     */
    void setSimulationType(SimulationType type);

    /**
     * @brief Set the gravity
     * @param gravity Gravity vector
     */
    void setGravity(const Eigen::Vector3d &gravity);

    /**
     * @brief Initialise the simulation
     */
    virtual void init() override;

    /**
     * @brief Perform one simulation step
     * @param dt Ellapsed time
     */
    virtual void step(std::chrono::microseconds dt) override;

    /**
     * @brief Deinitialise the simulation
     */
    virtual void cleanup() override;

    /**
     * @brief Simulation getter function
     * @return Smart pointer reference to the instance
     */
    inline const sofa::simulation::Simulation::SPtr & simulation() { return m_simulation; }

    /**
     * @brief Root node gettter function
     * @return Smart pointer reference to the root node
     */
    inline sofa::simulation::Node::SPtr & rootNode() { return m_rootNode; }

    /**
     * @brief Add plugin
     * @param name Name of the plugin
     */
    inline void addPlugin(const std::string &name) { m_plugins.insert(name); }

    /**
     * @brief Remove plugin
     * @param name Name of the plugin
     */
    void removePlugin(const std::string &name);

    /**
     * @brief Add root node component
     * @param name Name of the component
     * @param parameters Parameters of the constructed component
     */
    void addRootNodeComponent(const std::string &name, const std::map<std::string, std::string> &parameters);

    /**
     * @brief Remove root node component
     * @param name Name of the component
     * @param removeAllInstances Remove all instances or only the first encountered
     */
    void removeRootNodeComponent(const std::string &name, bool removeAllInstances = false);

    /**
     * @brief Get the synchronisation mutex
     */
    inline const std::shared_ptr<std::shared_mutex> & mutex() { return m_mutex; }

    /**
     * @brief Add a data repository
     * @param repo File repository
     */
    void addDataRepository(const FileRepository &repo);

    /**
     * @brief Add a plugin repository
     * @param repo Plugin repository
     */
    void addPluginRepository(const FileRepository &repo);

    /**
     * @brief Add physics body
     * @param builder Builder object containing the configuration of the body
     */
    const std::shared_ptr<PhysicalBody> addBody(PhysicalBody::Builder &builder);

    /**
     * @brief Set haptic interface used in the simulation
     * @param builder Builder object containing the configuration of the interface
     */
    const std::shared_ptr<HapticInterface> &addHapticInterface(HapticInterface::Builder &builder);

    /**
     * @brief Add articulation component
     * @param builder Builder object containing the configuation of the articulation
     * @return
     */
    const std::shared_ptr<Articulation> &addArticulation(Articulation::Builder &builder);

    /**
     * @brief Initialize all bodies in the simulation
     */
    void initBodies();

    /**
     * @brief Set a special collision rule
     *
     * Sets a rule for a pair of collision group, e.g. specific type of response,
     * specific paramater (friction, etc.). Set the pair of indices to
     * create a rule for two collision groups, use -1 to apply the rule to all
     * collision groups.
     *
     * @param collisionGroups A pair of collision groups that this rule applies to
     * @param response Type of response
     * @param params Response params
     */
    void setCollisionRule(const std::pair<int, int> collisionGroups,
                          const std::string &response,
                          const std::vector<std::pair<std::string, std::string>> &params = {});

private:
    std::shared_ptr<EventDispatcher> m_eventDispatcher;
    std::chrono::microseconds m_slack;
    sofa::simulation::Simulation::SPtr m_simulation;
    sofa::simulation::Node::SPtr m_rootNode;
    std::map<std::string, std::map<std::string, std::string>> m_rootNodeComponents;
    std::unordered_set<std::string> m_plugins;
    std::shared_ptr<std::shared_mutex> m_mutex;
    std::vector<std::shared_ptr<PhysicalBody>> m_bodies;
    std::vector<std::shared_ptr<HapticInterface>> m_hapticInterfaces;
    std::vector<std::shared_ptr<Articulation>> m_articulations;

    void populateRepository(const FileRepository &source, sofa::helper::system::FileRepository &destination);
};

}
