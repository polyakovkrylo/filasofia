/**
 * @file Node class declaration
 * @author Vladimir Poliakov
 */
#pragma once

#include <sofa/simulation/graph/DAGNode.h>

#include <string>

namespace filasofia {

/**
 * @brief The Node class
 *
 * Base class for physics bodies
 * implementing simple access to the SOFA
 * node object
 */
class Node
{
public:
    /**
     * @brief Node ctor
     * @param name Name of the node
     * @param parent parent of the node
     */
    Node(const std::string &name, sofa::simulation::Node::SPtr &parent);

    /**
     * @brief Node getter function
     * @return Smart pointer to the SOFA node
     */
    inline sofa::simulation::Node::SPtr & node() { return m_node; }

    /**
     * @brief Name getter
     * @return Name of the node
     */
    inline const std::string & name() { return m_name; }

private:
    /**
     * @brief Smart pointer to the SOFA node
     */
    sofa::simulation::Node::SPtr m_node;

    /**
     * @brief Name of the node
     */
    std::string m_name;
};

}
