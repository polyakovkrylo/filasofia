#pragma once

#include "node.h"

#include <Eigen/Geometry>

namespace filasofia {

class PhysicalBody;

class HapticInterface : public Node
{
public:
    enum class Device {
        Omega,
        Geomagic
    };

    class Builder {
    public:
        Builder(const std::string &name);

        Builder & device(Device device);

        Builder & scale(float scale);

        Builder & origin(const Eigen::Translation3d &translation, const Eigen::Quaterniond &rotation);

        Builder & controlledBody(const std::shared_ptr<PhysicalBody> &body);

        // geomagic requires device name
        std::shared_ptr<HapticInterface> build(sofa::simulation::Node::SPtr &parent);

    private:
        std::shared_ptr<HapticInterface> m_interface;

        std::shared_ptr<PhysicalBody> m_controlledBody;

        std::string m_name;

        Device m_device;

        float m_scale {1.0};

        Eigen::Translation3d m_translation;

        Eigen::Quaterniond m_rotation;

        void buildAttachment();
    };

    friend class HapticInterface::Builder;
private:
    HapticInterface(const std::string &name, sofa::simulation::Node::SPtr &parent);
};

}
