/**
 * @file The PhysicsBody class declaration
 * @author Vladimir Poliakov
 */
#pragma once

#include "node.h"

#include <memory>

#include <sofa/core/loader/MeshLoader.h>
#include <Eigen/Dense>

namespace filasofia {

class PhysicalBody : public Node
{
public:
    /**
     * @brief The mechanical model body type enum
     */
    enum class BodyType {
        Rigid,
        SoftMassSpring,
        SoftTetrahedronFem
    };

    /**
     * @brief The mass type enum
     */
    enum class MassType {
        Uniform,
        Diagonal
    };

    /**
     * @brief The ConstraintCorrection enum
     */
    enum class ConstraintCorrection {
        Uncoupled,
        Precomputed
    };

    enum class SolverType {
        None,
        Iterative,
        Direct
    };

    enum class IntegratorType {
        None,
        Implicit,
        Explicit
    };

    /**
     * @brief The SoftBodyParams struct
     */
    struct SoftBodyParams {
        double youngModulus             { 5000 };
        double poissonRatio             { 0.45 };
        double stiffness                { 100 };
        double damping                  { 0.01 };
    };

    /**
     * @brief The SolverParams struct
     */
    struct SolverParams {
        SolverType linearSolver         { SolverType::Iterative };
        IntegratorType integrator       { IntegratorType::Implicit };
        float precision                 { 1e-5 };
        int iterations                  { 25 };
        float rayleighStiffness         { 0.1f };
        float rayleighMass              { 1.5f };
        ConstraintCorrection correction { ConstraintCorrection::Uncoupled };
        float compliance                { 1e-5 };
    };

    // TODO: implement other types of mass
    /**
     * @brief The MechanicalModelParams struct
     */
    struct MechanicalModelParams {
        BodyType type                   { BodyType::Rigid };
        MassType massType               { MassType::Uniform };
        bool dynamicTopology            { false };
        float tearingThreshold          { std::numeric_limits<float>::max() };
        double mass                      { 0.0f };
        int numberOfElements                { 1 };
        SolverParams solverParams;
        SoftBodyParams softBodyParams;
    };

    /**
     * @brief The CollisionModelFlags enum
     */
    enum CollisionModelFlags {
        Point       = 0x1,
        Line        = 0x1 << 1,
        Triangle    = 0x1 << 2,
    };

    /**
     * @brief The CollisionModelParams struct
     */
    struct CollisionModelParams  {
        int primitives { Point | Line | Triangle };
        float proximity {0};
        int group {0};
        bool selfCollision {false};
        bool cutting {false};
    };

    /**
     * @brief Body attachment parameters
     */
    struct AttachmentParams {
        std::vector<uint> indicesFrom;
        std::vector<uint> indicesTo;
        bool barycentric {false};
        float linearStiffness {-1.0};
        float angularStiffness {-1.0};
    };

    /**
     * @brief Body type getter
     * @return type of the body
     * @sa BodyType
     */
    inline BodyType type() { return m_type; }

    /**
     * @brief The dynamic topology flag getter
     * @return Whether the body has dynamic topology (is modifiable)
     */
    inline bool hasDynamicTopology() { return m_dynamicTopology; }

    void init();

    class Builder {
    public:
        typedef std::array<float, 6> AABB;

        /**
         * @brief Builder ctor
         * @param bodyName name of the body
         * @param parent parent node of the body
         */
        Builder(const std::string &bodyName);

        /**
         * @brief Set the mechanical model
         * @param filename File name of the mesh
         * @return reference to the builder
         * @todo add custom mesh function
         */
        Builder & mechanicalModel(const MechanicalModelParams &params, const std::string &filename = "");

        /**
         * @brief Attach the body to another body
         *
         * Attaches this body to another one
         *
         * @param body Body, to which this body should be attached
         * @param params Attachment parameters
         * @sa AttachmentParams
         */
        Builder & attach(const std::shared_ptr<PhysicalBody> &body,
                         const AttachmentParams &params);

        /**
         * @brief Fixate an area of the created body
         * @param roi Box defining the region of interest
         * @param stiffness Stiffness of attachment (negative for the fixed constraint)
         * @return reference to the builder
         */
        Builder & pinArea(const std::vector<AABB> &roi, const double &stiffness = -1.0);

        /**
         * @brief Set the collision model builder
         * @return reference to the builder
         */
        Builder & collisionModel(const CollisionModelParams &params, const std::string &filename);

        /**
         * @brief Map to another physical body
         * @param source Source physical body
         * @return reference to the builder
         */
        Builder & map(const std::shared_ptr<PhysicalBody> &source, int index=0);

        /**
         * @brief Set the initial translation
         * @param translation Initial position vector
         * @return reference to the builder
         */
        Builder & translation(const Eigen::Vector3d &translation);

        /**
         * @brief Add a force feedback component
         * @param scale Force feedback coefficient
         * @return reference to the builder
         */
        Builder & forceFeedback(float scale);

        /**
         * @brief Build the body
         * @return Unique pointer to the body
         */
        std::shared_ptr<PhysicalBody> build(sofa::simulation::Node::SPtr &parent);

    private:
        /**
         * @brief Smart pointer to the body being built
         */
        std::shared_ptr<PhysicalBody> m_body;

        /**
         * @brief Body name
         */
        std::string m_bodyName;


        /**
         * @brief Mechanical model mesh file name
         */
        std::string m_mechanicalModel;

        /**
         * @brief Mechanical model parameters
         */
        MechanicalModelParams m_mechanicalModelParams;

        /**
         * @brief Collision model mesh file name
         */
        std::map<std::string, CollisionModelParams> m_collisionModels;

        /**
         * @brief Attached areas
         */
        std::vector<std::pair<std::shared_ptr<PhysicalBody>, AttachmentParams>> m_attachments;

        /**
         * @brief Pinned areas
         */
        std::vector<std::pair<std::vector<AABB>, float>> m_pinnedAreas;

        /**
         * @brief The body to which the current body is mapped
         */
        std::pair<std::shared_ptr<PhysicalBody>, int> m_mappedTo;

        /**
         * @brief Initial translation
         */
        Eigen::Vector3d m_translation { 0.0, 0.0, 0.0 };

        /**
         * @brief Force feedback
         */
        float m_forceFeedback {0.0};

        /**
         * @brief Build mechanical model
         */
        void buildMechanicalModel();

        /**
         * @brief Build collision model
         */
        void buildCollisionModels();

        /**
         * @brief Build attachment areas
         */
        void buildAttachments();

        /**
         * @brief Import mesh and create topology
         *
         * Creates necessary components to load the mesh and
         * import its topology
         *
         * @param parent Parent node the topology
         * @param filename Filename of the imported mesh
         * @return Smart pointer to the created topology
         * @note Supports .vtu, .stl, and .obj files
         */
        sofa::core::objectmodel::BaseObject::SPtr createMeshLoader(const sofa::simulation::Node::SPtr &parent, const std::string &filename);
    };

    friend class PhysicalBody::Builder;

private:
    /**
     * @brief PhysicsBody ctor
     * @param name Name of the body
     * @param parent Parent of the body node
     */
    PhysicalBody(const std::string &name, sofa::simulation::Node::SPtr & parent);

    /**
     * @brief Body type
     */
    BodyType m_type;

    /**
     * @brief Flag to indicate whether the body is modifiable
     */
    bool m_dynamicTopology;

    /**
     * @brief Collision model node
     *
     * Collision model, its mesh and mechanical model
     * should be stored in a dedicated node.
     * Refer to SOFA examples for detailed information
     */
    sofa::simulation::Node::SPtr m_collisionModel;

    /**
     * @brief Smart pointer to the mechanical mesh topology
     * @note Only valid for soft bodies
     */
    sofa::core::objectmodel::BaseObject::SPtr m_mechanicalMesh;

    /**
     * @brief Smart pointer to the collision mesh topology
     * @note Only valid for bodies with a collision model
     */
    sofa::core::objectmodel::BaseObject::SPtr m_collisionMesh;
};

}
