#pragma once

#include "physics/node.h"

#include <Eigen/Dense>

namespace filasofia {

class PhysicalBody;

class Articulation : public Node
{
public:

    enum class JointType {
        Revolute,
        Prismatic
    };

    enum ArticulationProcess {
        EulerAngles = 0,
        AttachedToParent = 1,
        AttachedToChild = 2
    };

    struct JointConfiguration {
        JointType type { JointType::Prismatic };
        ArticulationProcess process { ArticulationProcess::EulerAngles };
        Eigen::Vector3d parentOffset { 0.0, 0.0, 0.0 };
        Eigen::Vector3d childOffset { 0.0, 0.0, 0.0 };
        Eigen::Vector3d axis {0, 0, 1};
        float stiffness {100};
        float contactCompliance {10};
        int parentIndex {-1};
        int childIndex {-1};
    };

    class Builder {
    public:
        Builder(const std::string &name);

        Builder & joint(JointConfiguration &joint);

        Builder & controlledBody(const std::shared_ptr<PhysicalBody> &body);

        Builder & attachedTo(const std::shared_ptr<PhysicalBody> &body);

        std::shared_ptr<Articulation> build(sofa::simulation::Node::SPtr &parent);

    private:
        std::shared_ptr<Articulation> m_articulation;
        std::string m_name;
        std::vector<JointConfiguration> m_joints;
        std::shared_ptr<PhysicalBody> m_controlledBody;
        std::shared_ptr<PhysicalBody> m_attachment;
    };

    friend class Builder;

private:
    Articulation(const std::string &name, sofa::simulation::Node::SPtr &parent);
};

}

