/**
 * \file PeriodicTask class decalration
 * \author Vladimir Poliakov
 */
#pragma once

#include <memory>
#include <thread>

namespace filasofia {

/**
 * @brief PeriodicTask abstract class
 *
 * Encapsulates thread execution behaviour
 */
class PeriodicTask
{
 public:
  /**
   * @brief Constructor
   * 
   * @param period Period of repetition
   */
  PeriodicTask(std::chrono::microseconds period, bool realTime = false);

  /** @brief  Virtual destructor */
  virtual ~PeriodicTask() = default;

  /**
   * @brief Start execution
   *
   * Call init()  and 
   * execute step() with m_period interval
   */
  void run();

  /**
   * @brief Finish execution
   *
   * Stop thread execution
   * and call cleanup()
   */
  void finish();

  /** @brief Initialisation routine */
  virtual void init();

  /** @brief Initialization flag getter */
  inline bool isInitialized() { return m_initialized; }

  /** @brief Cleanup after finishing execution */
  virtual void cleanup() { m_initialized = false; }

  /** @brief Execute one iteration */
  virtual void step(std::chrono::microseconds) = 0;

  /** @brief Period setter function */
  inline void setPeriod(std::chrono::microseconds period) { m_period = period; }

  /** @brief Period getter function */
  inline std::chrono::microseconds period() { return m_period; }

  /** @brief Period getter function */
  inline std::chrono::microseconds getPeriod() { return m_period; }

  /**
   * @brief Real time flag setter function
   * If a task is real-time, sleep_until()
   * will be used instead of sleep_for()
   */
  inline void setRealTime(bool realTime) { m_realTime = realTime; }

  /** @brief Real time flag getter function */
  inline bool isRealTime() { return m_realTime; }
  
 private:
  std::unique_ptr<std::thread> m_thread;
  std::chrono::microseconds m_period;
  bool m_finished {false};
  bool m_initialized {false};
  bool m_realTime {false};
};

}
