#pragma once

#include <vector>
#include <boost/filesystem/path.hpp>

namespace filasofia {

class FileRepository {
public:
    FileRepository() = default;

    void addPath(const std::initializer_list<std::string> &paths, bool recursive);
    void addPath(std::string path, bool recursive = false);

    inline const std::vector<boost::filesystem::path> &paths() const { return m_paths; }

    const std::string find(const std::string &fileName);

private:
    std::vector<boost::filesystem::path> m_paths;
};

extern FileRepository DataRepository;

}
