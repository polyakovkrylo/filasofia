#pragma once

#include <memory>
#include <vector>

#include "mapping/abstractmapping.h"
#include "visual/visualserver.h"
#include "physics/physicsserver.h"
#include "event/eventdispatcher.h"
#include "mapping/abstractmapping.h"
#include "window/window.h"

namespace filasofia {

class EventDispatcher;

class Simulation
{
public:
    Simulation(const std::string &name, const std::chrono::milliseconds &visualUpdatePeriod = std::chrono::milliseconds(20));

    void exec();

    void map(const std::shared_ptr<filasofia::PhysicalBody> &physicsBody,
             const std::shared_ptr<Transform> &visualEntity);

    void map(const std::shared_ptr<filasofia::PhysicalBody> &physicsBody,
             const std::vector<std::shared_ptr<Transform>> &visualBodies,
             int startIndex = 0);

    inline filasofia::PhysicsServer & physicsServer() { return *m_physics; }
    inline VisualServer & visualServer() { return *m_visual; }
    inline EventDispatcher & eventDispatcher() { return *m_eventDispatcher; }

private:
    void processWindowEvents();
    void initMappings();
    void updateMappings();

    std::unique_ptr<filasofia::PhysicsServer> m_physics;
    std::unique_ptr<VisualServer> m_visual;
    std::unique_ptr<filasofia::window::AbstractWindow> m_window;

    std::vector<std::unique_ptr<AbstractMapping>> m_mappings;

    std::shared_ptr<EventDispatcher> m_eventDispatcher;

    std::chrono::milliseconds m_period;
    bool m_close {false};
};

}

