﻿#pragma once

#include "simulation/event/event.h"
#include "common/keyboard.h"

#include <memory>

#include <Eigen/Dense>

class SDL_Window;

namespace filasofia {
namespace window {

class AbstractWindow
{
public:
    struct Geometry {
        Eigen::Vector2i position;
        Eigen::Vector2i size;
    };

    AbstractWindow(const std::string &name, const Geometry &geometry);
    virtual ~AbstractWindow() = default;

    virtual void init() = 0;
    virtual void * nativeWindow() = 0;
    virtual std::unique_ptr<Event> poll() = 0;

    inline void setGeometry(Geometry geomtery) {m_geometry = geomtery;}
    inline const Geometry & geometry() const {return m_geometry;}

    inline void setName(const std::string &name) {m_name = name;}
    inline const std::string & name() const {return m_name;}

private:
    std::string m_name;
    Geometry m_geometry;
};

struct SDLWindowDestroyer
{
    void operator()(SDL_Window* w) const;
};

class SDLWindow : public AbstractWindow
{
public:
    SDLWindow(const std::string &name, const Geometry &geometry);
    virtual ~SDLWindow() override;

    virtual void init() override;
    virtual void * nativeWindow() override;
    virtual std::unique_ptr<Event> poll() override;

private:
    std::unique_ptr<SDL_Window, SDLWindowDestroyer> m_sdlWindow;
    ModifierKeyFlags mapModifiers(uint16_t sdl_modifiers);
};

}
}
