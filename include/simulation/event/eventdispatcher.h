#pragma once

#include <unordered_map>
#include <vector>
#include <memory>
#include <queue>
#include <mutex>

#include "event.h"
#include "common/periodictask.h"

namespace filasofia {

class EventObserver;

class EventDispatcher : public PeriodicTask
{
public:
    EventDispatcher(const std::chrono::microseconds &period = std::chrono::microseconds(0));

    void attach(const std::shared_ptr<EventObserver> &observer);
    void detach(const std::shared_ptr<EventObserver> &observer);
    void processEvent(const Event &event);
    void processEvents();
    void post(std::unique_ptr<Event> &event);

    virtual void step(std::chrono::microseconds) override;

private:
    std::unordered_map<Event::Type, std::vector<std::weak_ptr<EventObserver>>> m_observers;
    std::queue<std::unique_ptr<Event>> m_eventQueue;
    std::mutex m_mutex;
};

}
