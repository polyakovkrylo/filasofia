#pragma once

#include "event.h"
#include "common/keyboard.h"

namespace filasofia {

class KeyEvent : public Event
{
public:
    KeyEvent(const Event::Type &type,
             const KeyCode &key,
             const bool &pressed,
             const ModifierKeyFlags &modifiers = ModifierKey::None);

    virtual ~KeyEvent() = default;

    inline bool isPressed() const { return m_pressed; }
    inline const KeyCode &key() const { return m_key; }
    inline const ModifierKeyFlags & modifiers() const { return m_modifiers; }

private:
    KeyCode m_key;
    bool m_pressed;
    ModifierKeyFlags m_modifiers;
};

}
