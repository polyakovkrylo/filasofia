#pragma once

#include <vector>

#include "event.h"

namespace filasofia {
class EventObserver
{
public:
    EventObserver(std::initializer_list<Event::Type> filters);
    inline const std::vector<Event::Type> & filters() const { return m_filters; }
    virtual void processEvent(const Event &event) = 0;

private:
    std::vector<Event::Type> m_filters;
};

}
