#pragma once

namespace filasofia {
/**
 * @brief The Event class
 *
 * Implementing event-driven scripting mechanism
 */
class Event {
public:
    /**
     * @brief The type of the event
     */
    enum class Type {
        Empty,
        VisualUpdate,
        PhysicsUpdate,
        PhysicsInit,
        PhysicsCleanup,
        Quit,
        KeyPressed,
        KeyReleased,
        MousePressEvent,
        MouseReleaseEvent,
        MouseMoveEvent,
        CustomEvent1,
        CustomEvent2,
        CustomEvent3,
        CustomEvent4
    };

    /**
     * @brief The Event class ctor
     * @param type Triggered event
     * @param key Pressed/released key
     */
    Event(Type type);

    /**
     * @brief Virtual destructor
     */
    virtual ~Event() = default;

    /**
     * @brief Type getter
     * @return Type of the event
     */
    inline const Type & type() const { return m_type; }

private:
    Type m_type { Type::Empty };
};

}
