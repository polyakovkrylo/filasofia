#pragma once

#include "event.h"
#include "common/keyboard.h"

#include <Eigen/Dense>

namespace filasofia {

class MouseEvent : public Event
{
public:
    typedef Eigen::Vector2i Point;

    enum Button {
        None = 0x00,
        Left = 0x01,
        Right = 0x02,
        Middle = 0x04,
    };

    typedef uint8_t ButtonFlags;

    MouseEvent(const Event::Type &type,
               const ButtonFlags &buttons,
               const Point &position,
               const ModifierKeyFlags &modifiers);

    virtual ~MouseEvent() = default;

    inline const ButtonFlags & buttons() const { return m_buttons; }
    inline const Point & position() const { return m_position; }
    inline const ModifierKeyFlags & modifiers() const { return m_modifiers; }

private:
    ButtonFlags m_buttons;
    Point m_position;
    ModifierKeyFlags m_modifiers;
};

}

