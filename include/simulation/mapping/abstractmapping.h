/**
 * @file The AbstractMapping class declaration
 * @author Vladimir Poliakov
 */
#pragma once

#include <memory>
#include <shared_mutex>

#include "physics/physicalbody.h"
#include "visual/visualbody.h"

namespace filasofia {

class AbstractMapping
{
public:
    /**
     * @brief AbstractMapping ctor
     */
    AbstractMapping(const std::shared_ptr<PhysicalBody> &physicsBody,
                    const std::shared_ptr<std::shared_mutex> &mutex);

    virtual ~AbstractMapping() = default;

    /**
     * @brief Update visual body
     */
    virtual void update() = 0;

    virtual void init() = 0;

protected:
    std::weak_ptr<PhysicalBody> m_physicsBody;
    std::shared_ptr<std::shared_mutex> m_mutex;
};

}
