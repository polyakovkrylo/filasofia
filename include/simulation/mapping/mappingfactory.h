#pragma once

#include "abstractmapping.h"

namespace filasofia {

class MappingFactory {
public:
    static std::unique_ptr<AbstractMapping> create(const std::shared_ptr<PhysicalBody> &physicsBody,
                                                   const std::shared_ptr<Transform> &visualEnity,
                                                   const std::shared_ptr<std::shared_mutex> &mutex);

    static std::unique_ptr<AbstractMapping> create(const std::shared_ptr<PhysicalBody> &physicsBody,
                                                   const std::vector<std::shared_ptr<Transform>> &visualEntities,
                                                   const std::shared_ptr<std::shared_mutex> &mutex,
                                                   size_t startIndex = 0);
};

}
