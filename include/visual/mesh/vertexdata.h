#pragma once

#include <math/vec3.h>
#include <math/vec4.h>

namespace filasofia {
/**
 * @brief The Vertex struct
 */
struct VertexData {
    /** @brief Position coordinates */
    filament::math::float3 position;

    /** @brief Tangent orientation */
    filament::math::short4 tangent;

    /** @brief UV coordinates */
    filament::math::float2 uv { 0.0, 0.0};

    /** @brief Normal orientation */
    filament::math::float3 normal { 0.0, 0.0, 1.0 };
};
}
