#pragma once

#include "vertexdata.h"

#include <vector>
#include <set>

namespace filasofia {

/**
 * @brief The VisualMeshContainer class
 *
 * This is a base class that contains vertex and index vectors,
 * among with some utilities to update their vector attributes
 */
class VisualMeshContainer
{
public:
    VisualMeshContainer(int refinementLevel = 0);
    VisualMeshContainer(const std::vector<VertexData> &vertices, const std::vector<uint16_t> &indices, int refinementLevel = 0);
    virtual ~VisualMeshContainer() = default;

    /**
     * @brief Initialisation of vertex and index buffers
     */
    virtual void init() = 0;

    /**
     * @brief Refeinement level getter function
     * @return refinement level (number of subdivisions)
     */
    inline int refinementLevel() const { return m_refinementLevel; }

    /**
     * @brief Set refinement level
     * @param refinementLevel number of subdivisions
     * @note Can only be called before init()
     */
    inline void setRefinementLevel(int refinementLevel) { m_refinementLevel = refinementLevel; }

    /**
     * @brief Vertex vector setter function
     * @param vertices Vector of vertices
     */
    inline void setVertices(const std::vector<VertexData> &vertices) { m_vertices = vertices; }

    /**
     * @brief Vertex vector getter function
     * @return const reference to the vertex vector
     */
    inline const std::vector<VertexData> & vertices() const { return m_vertices; }

    /**
     * @brief Index vector setter function
     * @param indices Vector of indices
     */
    inline void setIndices(const std::vector<uint16_t> &indices) { m_indices = indices; }

    /**
     * @brief Index vector getter function
     * @return Vector of indices
     */
    inline const std::vector<uint16_t> & indices() const { return m_indices; }

    /**
     * @brief Remove vertices from the mesh
     *
     * Removes vertices and associated triangles from the mesh and updates
     * the index vector accordingly
     *
     * @param vertexIndices The vector of indices of vertices to be removed
     * @note The greater comparator is used to arrange the indices in descending order
     */
    virtual void removeVertices(const std::vector<uint16_t> &vertexIndices);

    /**
     * @brief Add new vertices
     * @param vertices Vector of vertices
     */
    virtual void addVertices(const std::vector<filament::math::float3> &positions);

    /**
     * @brief Set individual vertex
     * @param id Index of the vertex
     * @param vertex Vertex
     */
    inline void setVertex(unsigned long id, const VertexData vertex) { m_vertices.at(id) = vertex; }

    /**
     * @brief setVertexPosition
     * @param id Index of the vertex
     * @param position New Position
     */
    inline void setVertexPosition(unsigned long id, const filament::math::float3 &position) { m_vertices.at(id).position = position; }

    /**
     * @brief Set one index
     * @param id Index of the element in the index vector
     * @param value Value of the element(index)
     */
    inline void setIndex(unsigned long id, uint16_t value) { m_indices.at(id) = value; }

    /**
     * @brief Remove triangles from the mesh
     *
     * Removes triangles from the mesh.
     *
     * @param triangleIndices The vector of indices of triangles to be removed
     * @note Each element of triangleIndices should be a multiplication of three
     */
    virtual void removeTriangles(const std::vector<uint16_t> &triangleIndices);

    /**
     * @brief Add triangles to the mesh
     * @param indices Vector of indices per each new triangle
     */
    virtual void addTriangles(const std::vector<uint16_t> & indices);

    /**
     * @brief Set seams vector
     * @param seams vetor containing vertex pairs for each seam
     */
    inline void setSeams(const std::vector<std::vector<uint16_t>> &seams) { m_seams = seams; }

    /**
     * @brief Get seams vector
     * @return Seams vector
     */
    inline const std::vector<std::vector<uint16_t>> & seams() const { return m_seams; }

    /**
     * @brief Update vertex and index buffers
     */
    virtual void update() = 0;

    /**
     * @brief Recalculate tangents of the model
     */
    void recalculateTangents();

    /**
     * @brief Recalculate normals of the model
     */
    virtual void recalculateNormals();

    /**
     * @brief Find (double) seam vertices and add them to the seam array
     */
    void populateSeams();

protected:
    std::vector<VertexData> m_vertices;
    std::vector<uint16_t> m_indices;
    int m_refinementLevel{0};
    std::vector<std::vector<uint16_t>> m_seams;
};

}
