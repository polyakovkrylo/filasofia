/**
 * @file VisualMesh class declaration
 * @author Vladimir Poliakov
 */
#pragma once

#include "visual/filament_raii.h"
#include "visualmeshcontainer.h"
#include "visualmeshrefiner.h"

#include <filament/VertexBuffer.h>
#include <filament/IndexBuffer.h>
#include <filament/Box.h>

#include <vector>
#include <functional>

namespace filasofia {

/**
 * @brief The VisualMesh class
 */
class VisualMesh : public VisualMeshContainer
{
public:
    typedef std::function<void(void)> TopologyUpdateCallback;

    VisualMesh(int refinementLevel = 0);
    VisualMesh(const std::vector<VertexData> &vertices, const std::vector<uint16_t> &indices, int refinementLevel = 0);
    VisualMesh(VisualMesh &rhs);
    VisualMesh operator= (VisualMesh &rhs);
    virtual ~VisualMesh() = default;

    /**
     * @brief Initialisation of vertex and index buffers
     * @warning m_engine should be set before initialisation
     */
    virtual void init() override;

    /**
     * @brief Engine setter function
     * @param engine Engine
     */
    inline void setEngine(std::shared_ptr<filament::Engine> &engine) { m_engine = engine; }

    /**
     * @brief Vertex buffer getter function
     * @return Unique pointer to the vertex buffer
     */
    inline const FilamentScopedPointer<filament::VertexBuffer> & vertexBuffer() { return m_vertexBuffer; }

    /**
     * @brief Index buffer getter function
     * @return Unique pointer to the index buffer
     */
    inline const FilamentScopedPointer<filament::IndexBuffer> & indexBuffer() { return m_indexBuffer; }

    /**
     * @brief Update vertex and index buffers
     */
    virtual void update() override;

    /**
     * @brief Reimplemented from VisualMeshContainer::removeVertices
     *
     * Removes vertices and associated triangles from the mesh and updates
     * the index vector accordingly. If the mesh uses a refiner,
     * also updates its topology.
     *
     * @param vertexIndices The vector of indices of vertices to be removed
     */
    virtual void removeVertices(const std::vector<uint16_t> &vertexIndices) override;

    /**
     * @brief Add new vertices
     * @param vertices Vector of vertices
     */
    virtual void addVertices(const std::vector<filament::math::float3> &positions) override;

    /**
     * @brief Reimplemented from VisualMeshContainer::removeTriangles
     *
     * Removes triangles from the mesh. If the mesh uses a refiner,
     * also updates its topology.
     *
     * @param triangleIndices The vector of indices of triangles to be removed
     * @note Each element of triangleIndices should be a multiplication of three
     */
    virtual void removeTriangles(const std::vector<uint16_t> &triangleIndices) override;

    /**
     * @brief Add triangles to the mesh
     * @param indices Vector of indices per each new triangle
     */
    virtual void addTriangles(const std::vector<uint16_t> & indices) override;

    /**
     * @brief Get the number of indices in the rendered mesh
     * @return Number of indices
     */
    inline size_t indexCount() { return m_container->indices().size(); }

    /**
     * @brief Topology update callback setter function
     * @param cb Callback function
     */
    inline void setTopologyUpdateCallback(const TopologyUpdateCallback &cb) { m_topologyUpdateCallback = cb;}


private:
    void initIndexBuffer(size_t indexCount);
    void initVertexBuffer(size_t vertexCount);

    std::shared_ptr<filament::Engine> m_engine = nullptr;
    FilamentScopedPointer<filament::VertexBuffer> m_vertexBuffer = nullptr;
    FilamentScopedPointer<filament::IndexBuffer> m_indexBuffer = nullptr;

    VisualMeshContainer *m_container { this };
    TopologyUpdateCallback m_topologyUpdateCallback;

    std::unique_ptr<VisualMeshRefiner> m_refiner;
    bool m_topologyUpdateRequired {false};
};

}
