#pragma once

#include "visualmeshcontainer.h"

#include <vector>
#include <memory>

#include <opensubdiv/far/topologyDescriptor.h>
#include <opensubdiv/far/primvarRefiner.h>

namespace filasofia {

class VisualMesh;

class VisualMeshRefiner : public VisualMeshContainer
{
public:
    VisualMeshRefiner(VisualMesh *coarseMesh, int refineLevel = 2);
    virtual ~VisualMeshRefiner() = default;

    virtual void init() override;
    virtual void update() override;

private:
    VisualMesh *m_coarseMesh;

    // coarse mesh indices with "sawn" seams
    // (each vertex is only represented once)
    std::vector<int> m_coarseMeshGridIndices;

    // face varying vertices
    std::vector<int> m_coarseMeshFVarIndices;

    std::unique_ptr<OpenSubdiv::Far::TopologyRefiner> m_refiner;
};
}

