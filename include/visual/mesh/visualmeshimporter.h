#pragma once

#include "visualmesh.h"

namespace filasofia
{

    VisualMesh importVisualMesh(const std::string &filename, int refinementLevel = 0);

};

