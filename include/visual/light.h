/**
 * @file Light class declaration
 * @author Vladimir Poliakov
 */
#pragma once

#include "visual/transform.h"

#include <filament/LightManager.h>

namespace filasofia {

/**
 * @brief The Light class
 *
 * The Light class is an appropriation
 * of the FilamentScopedEntity that
 * ensures destruction of the light
 * with the light manager
 */
class Light : public Transform
{
public:
    /**
     * @brief Light ctor
     *
     * Light ctor accepts the LightManager builder
     * to pass the parameters of the light
     *
     * @param engine Shared pointer to the engine
     * @param builder Filament light build
     */
    Light(std::shared_ptr<filament::Engine> &engine, filament::LightManager::Builder &builder);

    virtual void update() override;
};

}
