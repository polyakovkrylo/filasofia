#pragma once

#include "visual/transform.h"

namespace filasofia {

class Camera : public Transform
{
public:
    Camera(const std::shared_ptr<filament::Engine> engine);

    inline filament::Camera * instance() { return m_camera; }

private:
    // filament deletes the camera component when its entity is deleted
    filament::Camera *m_camera;
};

}
