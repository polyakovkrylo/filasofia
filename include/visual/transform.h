 #pragma once

#include "visual/filament_raii.h"

#include "math/mat4.h"

#include <Eigen/Geometry>

namespace filasofia {

class Transform : public FilamentScopedEntity
{
public:
    Transform(Transform *parent = nullptr);
    Transform(utils::Entity &&entity,
              std::shared_ptr<filament::Engine> engine,
              Transform *parent = nullptr);
    Transform(const std::shared_ptr<filament::Engine> &engine,
              Transform *parent = nullptr);

    ~Transform();

    filament::math::mat4f transformMatrix();
    void setTransformMatrix(const filament::math::mat4f &transformMatrix);
    void setTransformMatrix(const std::array<float,16> &transformMatrix);

    void setFromPose(const Eigen::Translation3f &translation, const Eigen::Quaternionf &rotation);
    void translate(const Eigen::Vector3f translation);
    void rotate(const Eigen::Quaternionf rotation);

    void addChild(Transform *child);

    void removeChild(Transform *child);

    void setParent(Transform *parent);

    virtual void update();

private:
    filament::math::mat4f m_transformMatrix;
    Transform *m_parent {nullptr};
    std::vector<Transform *> m_children;

    filament::math::vec3<float> toFilamentVec3(const Eigen::Vector3f &vec);
    filament::math::quatf toFilamentQuat(const Eigen::Quaternionf &quat);
};

}
