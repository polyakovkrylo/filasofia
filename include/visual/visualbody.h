/**
 * @file VisualBody class declaration
 * @author Vladimir Poliakov
 */
#pragma once

#include "transform.h"
#include "mesh/visualmesh.h"

#include <filament/Material.h>
#include <filament/MaterialInstance.h>
#include <filament/Texture.h>
#include <filament/TextureSampler.h>

#include <memory>
#include <unordered_map>
#include <mutex>

namespace filasofia {

/**
 * @brief The VisualBody class
 *
 * The VisualBody class encapsulates all entities
 * needed to render a visual body:
 * MaterialInstance, TextureSampler and VisualMesh instances
 */
class VisualBody : public Transform
{
public:
    /**
     * @brief The Builder class
     *
     * Builder class is used for sequential
     * construtction of the VisualBody instance
     */
    class Builder {
    public:
        Builder(const std::string &name);

        /**
         * @brief Set mesh for the visual body
         * @param mesh Mesh containg Vertex and index vectors
         * @return Reference to the builder
         */
        Builder & mesh(const VisualMesh &mesh);

        /**
         * @brief Set material
         * @param material Shared pointer to the material
         * @return Reference to the builder
         */
        Builder & material(const std::shared_ptr<filament::Material> &material);

        /**
         * @brief Add texture
         *
         * This function can be called multiple times to add textures
         *
         * @param name Name of the texture according to material parameters
         * @param texture Shared pointer to the texture
         * @return Reference to the builder
         */
        Builder & addTexture(const std::string &name, const std::shared_ptr<filament::Texture> &texture);

        /**
         * @brief Build the body
         * @param engine Shared pointer to the engine instance
         * @return Unique pointer to the body
         */
        std::shared_ptr<VisualBody> build(std::shared_ptr<filament::Engine> engine);

    private:
        std::shared_ptr<VisualBody> m_body;
    };

    friend class VisualBody::Builder;

    /**
     * @brief Initialise the instance
     *
     * Initialise mesh buffers, material instance, texture sampler,
     * and create the renderable entity
     */
    void init();

    /**
     * @brief Name getter
     * @return The name of the body
     */
    inline const std::string & name() { return m_name; }

    /**
     * @brief Acquire the visual mesh to perform topological changes
     *
     * Returns the reference to the mesh and locks it to prevent multiple access
     *
     * @return Reference to the mesh
     * @warning submitMesh() should be called once the changes are finished
     * @throw runtime_error if the mesh is already acquired by another entity
     */
    VisualMesh & acquireMesh();

    /**
     * @brief Submit the mesh changes
     *
     * This function should be called in pair with acquireMesh() to perform topological changes
     */
    void submitMesh();

    /**
     * @brief Mesh getter function
     * @return Reference to the mesh
     */
    inline const VisualMesh & mesh() const { return m_mesh; }

    /**
     * @brief Material instance getter function
     * @return Unique pointer reference to the material instance
     */
    inline const FilamentScopedPointer<filament::MaterialInstance> & materialInstance() const { return m_materialInstance; }

    /**
     * @brief Texture getter function
     * @param name Name of the texture according to material parameters
     * @return Shared pointer reference to the texture
     */
    inline const std::shared_ptr<filament::Texture> & texture(const std::string &name) { return m_textures[name]; }

    /**
     * @brief Texture sampler getter function
     * @return Reference to the sampler
     */
    inline filament::TextureSampler & textureSampler() { return m_textureSampler; }

    virtual void update() override;

private:
    VisualBody(const std::string &name);
    filament::Box calculateAabb();
    void updateAabb();

    std::string m_name;
    std::shared_ptr<filament::Engine> m_engine;
    std::shared_ptr<filament::Material> m_material;
    FilamentScopedPointer<filament::MaterialInstance> m_materialInstance;
    std::unordered_map<std::string, std::shared_ptr<filament::Texture>> m_textures;
    filament::TextureSampler m_textureSampler;
    VisualMesh m_mesh;
    std::mutex m_mutex;
};

}
