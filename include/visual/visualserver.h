/**
 * @file VisualServer class declaration
 * @author Vladimir Poliakov
 */
#pragma once

#include "light.h"
#include "camera.h"
#include "visualbody.h"
#include "filament_raii.h"

#include <vector>
#include <unordered_map>
#include <string>

#include <filament/Engine.h>
#include <filament/SwapChain.h>
#include <filament/Renderer.h>
#include <filament/View.h>
#include <filament/Camera.h>
#include <filament/Scene.h>
#include <filament/Material.h>
#include <filament/Texture.h>
#include <filament/Skybox.h>
#include <filament/IndirectLight.h>

namespace filasofia {

/**
 * @brief The VisualServer class
 */
class VisualServer
{
public:
    /**
     * @brief VisualServer
     * @param window Pointer to the native window
     * @param windowWidth Window width in pixels
     * @param windowHeight Window height in pixels
     * @param createDefaultViewAndCamera If true, the default view and attached camera will be created
     */
    VisualServer(void *window, uint32_t windowWidth, uint32_t windowHeight, bool createDefaultViewAndCamera = true);

    ~VisualServer();

    /**
     * @brief Create a view and a camera for this view
     * @param name Name of the components, same for the camera and the view
     */
    void createViewAndCamera(const std::string &name);

    /** @brief Render the scene*/
    void render();

    /**
     * @brief Create skybox
     * @param filename Name of the skybox texture file
     */
    void createSkybox(const std::string &filename);

    /**
     * @brief Create IBL
     * @param filename Name of the IBL texture file
     */
    void createIbl(const std::string &filename, float intensity);

    /**
     * @brief Create new view
     * @param name Name of the view
     * @param viewport Relative viewport settings in range (0.0, 1.0): {x, y, width, height}
     * @return Created viewport
     */
    const FilamentScopedPointer<filament::View> & createView(const std::string &name,
                                                             const filament::math::float4 &viewport = {0.0f, 0.0f, 1.0f, 1.0f});
    /**
     * @brief View component getter
     * @param name Name of the view
     * @return Pointer to the view
     */
    inline const FilamentScopedPointer<filament::View> & view(const std::string &name = "default") { return m_views[name]; }

    /**
     * @brief Create new camera
     * @param name Name of the camera
     * @param fov Field of view in degrees
     * @param aspect Aspect ratio
     * @param near Near z-plane
     * @param far Far z-plane
     * @return Created camera
     */
    const std::shared_ptr<Camera> & createCamera(const std::string &name,
                                                 double fov = 90,
                                                 double aspect  = 16.0 / 9.0,
                                                 double near = 0.01,
                                                 double far = 100.0);

    /**
     * @brief Camera component getter
     * @param name Name of the camera
     * @return Camera component
     */
    inline const std::shared_ptr<Camera> & camera(const std::string &name = "default") { return m_cameras[name]; }

    /**
     * @brief Add material
     * @param name Name of the material
     * @param fileName Name of the file storing data
     */
    void addMaterial(const std::string &name, const std::string &fileName);

    /**
     * @brief material Get material
     * @param name Name of the material
     * @return shared pointer to the material
     */
    inline std::shared_ptr<filament::Material> & material(const std::string &name) { return m_materials[name]; }

    /**
     * @brief Add texture
     * @param name Name of the texture
     * @param fileName Name of the image file
     */
    void addTexture(const std::string &name, const std::string &fileName);

    /**
     * @brief texture Get texture
     * @param name Name of the texture
     * @return shared pointer to the texture
     */
    inline std::shared_ptr<filament::Texture> & texture(const std::string &name) { return m_textures[name]; }

    /**
     * @brief Create visual body and add it to the scene
     * @param builder Builder object containing configuration of the body
     */
    // TODO: For some reason, using a reference causes crashes
    const std::shared_ptr<VisualBody> addVisualBody(VisualBody::Builder &builder);

    /**
     * @brief Add a transform
     *
     * A transform is an empty visual entity that stores a pose and can
     * be used, for instance, as an offset for a child visual body
     *
     * @param transform Pose of the created transform
     * @return pointer to the created transform
     */
    const std::shared_ptr<Transform> addTransform(const Eigen::Translation3f &translation, const Eigen::Quaternionf &rotation);

    /**
     * @brief Create light and add it to the scene
     * @param builder Builder object containing configuration of the light
     */
    const std::shared_ptr<Light> & addLight(filament::LightManager::Builder &builder);

private:
    void *m_nativeWindowHandler;
    uint32_t m_windowWidth;
    uint32_t m_windowHeight;
    std::shared_ptr<filament::Engine> m_engine;
    FilamentScopedPointer<filament::SwapChain> m_swapChain;
    FilamentScopedPointer<filament::Renderer> m_renderer;
    FilamentScopedPointer<filament::Scene> m_scene;
    std::unordered_map<std::string, FilamentScopedPointer<filament::View>> m_views;
    std::unordered_map<std::string, std::shared_ptr<Camera>> m_cameras;
    std::unordered_map<std::string, std::shared_ptr<filament::Material>> m_materials;
    std::unordered_map<std::string, std::shared_ptr<filament::Texture>> m_textures;

    std::vector<std::shared_ptr<Light>> m_lights;
    std::vector<std::shared_ptr<VisualBody>> m_bodies;
    std::vector<std::shared_ptr<Transform>> m_transforms;

    FilamentScopedPointer<filament::Skybox> m_skybox;
    FilamentScopedPointer<filament::IndirectLight> m_ibl;
};

}
